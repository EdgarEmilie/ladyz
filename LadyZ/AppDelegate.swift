//
//  AppDelegate.swift
//  Salon
//
//  Created by Admin on 7/15/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func createHome() {
        let w = UIWindow(frame: UIScreen.main.bounds)
        
        self.window = w
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as? LadieZTabViewController
        
        w.rootViewController = homeVC
        w.makeKeyAndVisible()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = colour.backgraundColor
            appearance.titleTextAttributes = [.foregroundColor: UIColor.black, .font: UIFont.LadyzFont(name: .MontserratRegular, size: 18)]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.black, .font: UIFont.LadyzFont(name: .MontserratRegular, size: 14)]
            appearance.setBackIndicatorImage(UIImage(named: "chevron_left")!, transitionMaskImage: UIImage(named: "chevron_left")!)
            appearance.buttonAppearance.normal.titleTextAttributes = [.font: UIFont.LadyzFont(name: .MontserratRegular, size: 14)]
            appearance.doneButtonAppearance.normal.titleTextAttributes = [.font: UIFont.LadyzFont(name: .MontserratRegular, size: 14)]
            UINavigationBar.appearance().tintColor = .black
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
           
            
           

//            let BarButtonItemAppearance = UIBarButtonItem.appearance()
//            BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], for: .normal)
//            BarButtonItemAppearance.setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -200, vertical: 0), for:UIBarMetrics.default)
//
//            let barButton = UIBarButtonItem.appearance()
//            barButton.setTitleTextAttributes([.foregroundColor: UIColor.red,.font: UIFont.LadyzFont(name: .MontserratRegular, size: 18)], for: .normal)
        } else {
            UIBarButtonItem.appearance().tintColor = .black
            UINavigationBar.appearance().barTintColor = colour.backgraundColor
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.LadyzFont(name: .MontserratRegular, size: 18)]
            UINavigationBar.appearance().backgroundColor = colour.backgraundColor
            UINavigationBar.appearance().backIndicatorImage = UIImage(named: "chevron_left")
            UINavigationBar.appearance().barStyle = .default
            UINavigationBar.appearance().isTranslucent = false
            
            UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear, NSAttributedString.Key.font: UIFont.LadyzFont(name: .MontserratRegular, size: 14)], for: UIControl.State.normal)

            UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font : UIFont.LadyzFont(name: .MontserratRegular, size: 18) ], for: .highlighted)
            UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font : UIFont.LadyzFont(name: .MontserratRegular, size: 18) ], for: .focused)
//            UINavigationBar.appearance().
            
//            let BarButtonItemAppearance = UIBarButtonItem.appearance()
//            BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], for: .normal)
//            BarButtonItemAppearance.setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -200, vertical: 0), for:UIBarMetrics.default)
//
//
//            let barButton = UIBarButtonItem.appearance()
//                       barButton.setTitleTextAttributes([.foregroundColor: UIColor.red,.font: UIFont.LadyzFont(name: .MontserratRegular, size: 18)], for: .normal)
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

