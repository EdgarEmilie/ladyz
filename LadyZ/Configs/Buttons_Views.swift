//
//  Checkbox.swift
//  LadieZ
//
//  Created by Admin on 8/6/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit
import MaterialComponents.MaterialTextFields

let colour = ColourTheme()
// MARK: - CheakBOX
class CheckBox: UIButton {
    // Images
    let checkedImage = UIImage(named: "check_box_white")! as UIImage
    let uncheckedImage = UIImage(named: "check_box_outline")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action:#selector(buttonClicked(sender:)), for: UIControl.Event.touchUpInside)
        self.isChecked = false
        //self.isUserInteractionEnabled = false
    }
    
    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
// MARK: - Radio Button
class RadioButton: UIButton {
    var alternateButton:Array<RadioButton>?
    
    let checkedImage = UIImage(named: "radio_button_checked")! as UIImage
    let uncheckedImage = UIImage(named: "radio_button_unchecked")! as UIImage
    
    override func awakeFromNib() {
//        self.layer.cornerRadius = 5
//        self.layer.borderWidth = 2.0
//        self.layer.masksToBounds = true
    }
    
    func unselectAlternateButtons(){
        if alternateButton != nil {
            self.isSelected = true
            
            for aButton:RadioButton in alternateButton! {
                aButton.isSelected = false
            }
        }else{
            toggleButton()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        unselectAlternateButtons()
        super.touchesBegan(touches, with: event)
    }
    
    func toggleButton(){
        self.isSelected = !isSelected
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
}
    
class AlertLabel: UILabel {
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 2, left: 2, bottom: 2, right: 2)
        super.drawText(in: rect.inset(by: insets))
    }
    
}
class strikethroughLabel : UILabel{
    override func awakeFromNib() {
        let attributedString = NSMutableAttributedString(string:self.text ?? "")
        attributedString.addAttribute(NSAttributedString.Key.baselineOffset, value: 0, range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.thick.rawValue), range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor.gray, range: NSMakeRange(0, attributedString.length))
        
        attributedText = attributedString
    }
}
//////////////////////////
class MainButton : UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel?.font = UIFont.LadyzFont(name: .MontserratSemiBold, size: 16)
        setGradient(from: colour.purpleColor, to: colour.redColor)
//        layer.cornerRadius = self.frame.height / 2
//        layer.masksToBounds = false
//        layer.shadowColor = UIColor.darkGray.cgColor
//        layer.shadowOpacity = 0.5
//        layer.shadowOffset = CGSize(width: 2, height: 3)
//        layer.shadowRadius = 6
//        backgroundColor = colour.buttonColor
    }
}
class CornerRadusView : UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 10
        layer.masksToBounds = false
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.08).cgColor
        layer.shadowOpacity = 1
        layer.shadowRadius = 8
        layer.shadowOffset = CGSize(width: 0, height: 4)
//        layer.shadowColor = UIColor.lightGray.cgColor
//        layer.shadowOpacity = 0.8
//        layer.shadowOffset = CGSize(width: 0, height: 3)
//        layer.shadowRadius = 4
    }
}
// MARK: HighlightedButton
class HighlightedButton: UIButton {
    
    override var isHighlighted: Bool {
        didSet {
            if self.superview is CornerRadusView {
                self.superview?.backgroundColor = isHighlighted ? colour.buttonColor.withAlphaComponent(0.3) : .clear//UIColor(red:0.96, green:0.96, blue:0.99, alpha:1.0)
            }else {
                self.superview?.backgroundColor = isHighlighted ? colour.buttonColor.withAlphaComponent(0.3) : .clear
            }
        }
    }
}
// MARK: - Text field
class VRTextField : MDCTextField {
    var mc : MDCTextInputControllerUnderline?
    override func awakeFromNib() {
        super.awakeFromNib()
            mc = MDCTextInputControllerUnderline(textInput: self)
        mc?.floatingPlaceholderActiveColor = colour.buttonColor
        mc?.floatingPlaceholderNormalColor = colour.buttonColor
        mc?.activeColor = colour.buttonColor
            //mc?.textInput?.font = UIFont(name: "System", size: 22)
            //mc?.textInput?.placeholderLabel.font = UIFont(name: "System", size: 22)
            mc?.textInput?.underline?.lineHeight = 1
            //mc?.normalColor = UIColor.lightGray
            
            //mc?.leadingUnderlineLabelTextColor = UIColor.red
            //mc?.trailingUnderlineLabelTextColor = UIColor.red
            //mc?.inlinePlaceholderColor = UIColor.lightGray
        }
    func LightPlaceHolder () {
        mc?.inlinePlaceholderColor = UIColor.lightGray
    }
}
// MARK: - Search bar
public class VRSearchBar: UISearchBar {
    
    var preferredFont: UIFont?
    var preferredTextColor: UIColor?
    
    init(){
        super.init(frame: CGRect.zero)
    }
    
    func setUp(delegate: UISearchBarDelegate?,
               frame: CGRect?,
               barStyle: UISearchBar.Style,
               placeholder: String,
               font: UIFont?,
               textColor: UIColor?,
               barTintColor: UIColor?,
               tintColor: UIColor?,
                bckColor: UIColor?
        ){
        
        self.delegate = delegate
        self.frame = frame ?? self.frame
        self.searchBarStyle = searchBarStyle
        self.placeholder = placeholder
        self.preferredFont = font
        self.preferredTextColor = textColor
        self.barTintColor = barTintColor ?? self.barTintColor
        self.tintColor = tintColor ?? self.tintColor
        self.bottomLineColor = tintColor ?? UIColor.clear
        self.backgroundColor = bckColor ?? UIColor.clear
        sizeToFit()
        
        //        translucent = false
        //        showsBookmarkButton = false
        //        showsCancelButton = true
        //        setShowsCancelButton(false, animated: false)
        //        customSearchBar.backgroundImage = UIImage()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    let bottomLine = CAShapeLayer()
    var bottomLineColor = UIColor.clear
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        for view in subviews {
            if let searchField = view as? UITextField { setSearchFieldAppearance(searchField: searchField); break }
            else {
                for sView in view.subviews {
                    if let searchField = sView as? UITextField { setSearchFieldAppearance(searchField: searchField); break }
                }
            }
        }
        
        bottomLine.path = UIBezierPath(rect: CGRect(x: 0.0, y: 0.0, width: 0, height: 0)).cgPath//(rect: CGRect(0.0, frame.size.height - 1, frame.size.width, 1.0)).cgPath
        bottomLine.fillColor = bottomLineColor.cgColor
        layer.addSublayer(bottomLine)
    }
    
    func setSearchFieldAppearance(searchField: UITextField) {
        searchField.frame = CGRect(x: 0.0, y: 0.0, width: frame.size.width, height: frame.size.height )
        searchField.font = preferredFont ?? searchField.font
        searchField.textColor = preferredTextColor ?? searchField.textColor
        searchField.backgroundColor = UIColor.clear
        //backgroundImage = UIImage()
    }
    
}
class gradient : UIView {
    override func awakeFromNib() {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [UIColor.black.withAlphaComponent(0.4).cgColor, UIColor.black.withAlphaComponent(0.0).cgColor]
        
        self.layer.insertSublayer(gradient, at: 0)
    }
}
extension UIView {
    
    func setGradientWithFrame(from: UIColor, to : UIColor, frame : CGRect) {
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = [from.cgColor, to.cgColor]
        
        self.layer.insertSublayer(gradient, at: 0)
    }
    func setGradient (from: UIColor, to : UIColor) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [from.cgColor, to.cgColor]
       
        self.layer.insertSublayer(gradient, at: 0)
    }
    
}
extension UITextField {
    func placeholderColor(_ color: UIColor){
        var placeholderText = ""
        if self.placeholder != nil{
            placeholderText = self.placeholder!
        }
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor : color])
    }
}
// MARK: - NavigationItem(custom)
extension UIButton {
    convenience init(imageName: String,tintColor : UIColor) {
        self.init(type:UIButton.ButtonType.custom)
        setNavigationItem(imageName: imageName,tintColor : tintColor)
    }
    func setNavigationItem(imageName: String,tintColor : UIColor) {
        self.frame = CGRect(x: 0, y: 0, width: 25, height: 30)
        let origImage = UIImage(named: imageName);
        let image = origImage?.withRenderingMode(.alwaysTemplate)
        self.tintColor = tintColor
        self.setImage(image, for: .normal)
    }
}
// MARK: - BackButton
extension UIViewController {
    func addBackButton() {

        
        let btnLeftMenu: UIButton = UIButton.init(type:UIButton.ButtonType.custom)
        //button.setImage(UIImage(named: "search_white"), for: UIControl.State.normal)
        let origImage = UIImage(named: "chevron-left")
        let image = origImage?.withRenderingMode(.alwaysTemplate)
        btnLeftMenu.tintColor = UIColor.white
        btnLeftMenu.setImage(image, for: .normal)
        btnLeftMenu.setTitle("  ", for: .normal);
        btnLeftMenu.sizeToFit()
        btnLeftMenu.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    func addDarkBackButton() {
        let btnLeftMenu: UIButton = UIButton()
        let origImage = UIImage(named: "chevron-left");
        let image = origImage?.withRenderingMode(.alwaysTemplate)
        btnLeftMenu.tintColor = colour.buttonColor
        btnLeftMenu.setImage(image, for: .normal)
        btnLeftMenu.setTitle("  ", for: .normal);
        btnLeftMenu.sizeToFit()
        btnLeftMenu.addTarget(self, action: #selector (backButtonClick(sender:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func backButtonClick(sender : UIButton) {
        self.navigationController?.popViewController(animated: true);
    }
}
// MARK : -status bar color
extension UIView {
    func statusBarColor(Color: UIColor = UIColor.clear){//UIColor(red:0.24, green:0.23, blue:0.33, alpha:1.0)/*UIColor.black.withAlphaComponent(0.7)*/) {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            //statusBar.setGradient(from: colour.backgraundColor2.withAlphaComponent(0.4), to: colour.backgraundColor2.withAlphaComponent(0.3))
            statusBar.backgroundColor = Color
        }
    }
    //MARK : RoundCorners
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    func statusBarHeight() -> CGFloat {
        let statusBarSize = UIApplication.shared.statusBarFrame.height
        return statusBarSize
    }
}
extension UIImage {
    func isPixelColorISWhite(point: CGPoint, sourceView: UIView) -> Bool {
        let pixel = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: 4)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        
        context!.translateBy(x: -point.x, y: -point.y)
        
        sourceView.layer.render(in: context!)
        //pixel.deallocate(capacity: 4)
//        let color: UIColor = UIColor(red: CGFloat(pixel[0])/255.0,
//                                     green: CGFloat(pixel[1])/255.0,
//                                     blue: CGFloat(pixel[2])/255.0,
//                                     alpha: CGFloat(pixel[3])/255.0)
        if CGFloat(pixel[0])/255.0 > 0.66 && CGFloat(pixel[1])/255.0 > 0.66 && CGFloat(pixel[2])/255.0 > 0.66 {
            return true
        } else {
            return false
        }
    }
}

extension UINavigationBar
{
    /// Applies a background gradient with the given colors
    func apply(gradient colors : [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    /// Creates a gradient image with the given settings
    static func gradient(size : CGSize, colors : [UIColor]) -> UIImage?
    {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }
        
        // Create the Coregraphics gradient
        var locations : [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: 0.0, y: size.height), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
// MARK : -htem to string::: not usig
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
// MARK : -silencFrom
extension String {
    func sliceFrom(start: String, to: String) -> String? {
        if(start == "")
        {
            return (range(of: to)?.lowerBound).map { eInd in
                substring(with: startIndex..<eInd)
            }
        }
        return (range(of: start)?.upperBound).flatMap { sInd in
            (range(of: to, range: sInd..<endIndex)?.lowerBound).map { eInd in
                substring(with: sInd..<eInd)
            }
        }
    }
}

extension UIImage {
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    convenience init?(withContentsOfUrl url: URL) throws {
        let imageData = try Data(contentsOf: url)
        
        self.init(data: imageData)
    }
    
}
