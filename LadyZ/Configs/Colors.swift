//
//  Colors.swift
//  LadieZ
//
//  Created by Admin on 8/16/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

class ColourTheme
{
    class var colourTheame:Int {
        get {
            return UserDefaults.standard.integer(forKey: "colourTheame")
        }
        set(val) {
            UserDefaults.standard.set(val, forKey: "colourTheame")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var backgraundColor: UIColor
    var backgraundColor2: UIColor
    var darkColor: UIColor
    var buttonColor: UIColor
    var otherColor: UIColor
    var textColor: UIColor
    var redColor: UIColor
    var purpleColor: UIColor
    
    init()
    {
       
        backgraundColor = UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1);_ = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        backgraundColor2 = UIColor(red: 0.98, green: 0.94, blue: 0.94, alpha: 1);_ = #colorLiteral(red: 0.9764705882, green: 0.937254902, blue: 0.9411764706, alpha: 1)//#colorLiteral(red: 0.91, green: 0.25, blue: 0.58, alpha: 1)
        darkColor = UIColor(red: 0.47, green: 0.39, blue: 0.36, alpha: 1);_ = #colorLiteral(red: 0.4666666667, green: 0.3882352941, blue: 0.3607843137, alpha: 1)
        buttonColor = UIColor(red: 0.94, green: 0.81, blue: 0.78, alpha: 1);_ = #colorLiteral(red: 0.9411764706, green: 0.8117647059, blue: 0.7764705882, alpha: 1)
        otherColor = UIColor(red: 0.98, green: 0.74, blue: 0.39, alpha: 1);_ = #colorLiteral(red: 0.9843137255, green: 0.7411764706, blue: 0.3882352941, alpha: 1)
        textColor = UIColor(red:0.38, green:0.38, blue:0.38, alpha:1.0);_ = #colorLiteral(red: 0.3764705882, green: 0.3764705882, blue: 0.3764705882, alpha: 1)
        redColor = UIColor(red: 0.96, green: 0.31, blue: 0.37, alpha: 1);_ = #colorLiteral(red: 0.96, green: 0.31, blue: 0.37, alpha: 1)
        purpleColor = UIColor(red: 0.91, green: 0.25, blue: 0.58, alpha: 1);_ = #colorLiteral(red: 0.91, green: 0.25, blue: 0.58, alpha: 1)
    }
}
