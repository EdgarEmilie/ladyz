//
//  Fonts.swift
//  LadyZ
//
//  Created by Admin on 12/11/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIFont{
    enum font {
        case MontserratLight
        case MontserratRegular
        case MontserratMedium
        case MontserratSemiBold
        case MontserratBold
        
        case RobotoLight
        case RobotoRegular
        case RobotoMedium
        case RobotoBold
    }
    
    
    static func LadyzFont(name: font, size: CGFloat ) -> UIFont {
        switch name {
        case font.MontserratLight:
        return UIFont(name: "Montserrat-Light", size: size)! // ?? UIFont.systemFont(ofSize: size)
        case font.MontserratRegular:
        return UIFont(name: "Montserrat-Regular", size: size)!// ?? UIFont.systemFont(ofSize: size)
        case font.MontserratMedium:
        return UIFont(name: "Montserrat-Medium", size: size)!// ?? UIFont.systemFont(ofSize: size)
        case font.MontserratSemiBold:
        return UIFont(name: "Montserrat-SemiBold", size: size)!// ?? UIFont.systemFont(ofSize: size)
        case font.MontserratBold:
        return UIFont(name: "Montserrat-Bold", size: size)!// ?? UIFont.systemFont(ofSize: size)
            
        case font.RobotoLight:
        return UIFont(name: "Roboto-Light", size: size)!// ?? UIFont.systemFont(ofSize: size)
        case font.RobotoRegular:
        return UIFont(name: "Roboto-Regular", size: size)!// ?? UIFont.systemFont(ofSize: size)
        case font.RobotoMedium:
        return UIFont(name: "Roboto-Medium", size: size)!// ?? UIFont.systemFont(ofSize: size)
        case font.RobotoBold:
        return UIFont(name: "Roboto-Bold", size: size)!// ?? UIFont.systemFont(ofSize: size)
//        default:
//            return UIFont(name: "Roboto-Light", size: size) ?? UIFont.systemFont(ofSize: size)
        }
    }
}
