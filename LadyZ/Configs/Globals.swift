//
//  Globals.swift
//  LadieZ
//
//  Created by Admin on 8/25/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Foundation

class Globals {
    
    //public var StrypeKey = "pk_test_JbeA0FO1qfsMJCtDXRuahLHz"
    public var isTest : Bool = false
        public static var sharedInstance: Globals = Globals()
//        var userInfo:UserInfo = UserInfo()
        public var shearedSecret = "8ecbb2264c07404a82e335a56f5651cc"
        public enum RegisterPurchase : String {
            case tr1 = "tr1"
            case tr2 = "tr2"
            case tr3 = "tr3"
        }
        var app: String = ""
    
        private init()
        {
        }
        public var selectedLanguage: String {
    
            get {
                let ct = UserDefaults.standard.string(forKey: "selectedLanguage");
                if(ct != nil){
                    return ct!
                }else {
                    return "en"//NSLocale.system.identifier
                }
            }
            
            set(val) {
                UserDefaults.standard.set(val, forKey: "selectedLanguage")
                UserDefaults.standard.synchronize()
//                Swifternalization.setLanguage(lng: val)
            }
        }
    public var QRCode: String {
        get {
            let qr = UserDefaults.standard.string(forKey: "QRCode");
            if(qr != nil){
                return qr!
            } else {
                return ""//NSLocale.system.identifier
            }
        }
        
        set(val) {
            UserDefaults.standard.set(val, forKey: "QRCode")
            UserDefaults.standard.synchronize()
        }
    }
    public var isOnce: Bool{
        get {
            let o = UserDefaults.standard.string(forKey: "isOnce");
            if(o != nil){
                return false
            }else {
                return true//NSLocale.system.identifier
            }
        }
        
        set(val) {
            UserDefaults.standard.set(val, forKey: "isOnce")
            UserDefaults.standard.synchronize()
        }
    }
    public var server_time: Double?
    public var hallID: Int?
    public var IntercomLogin : Bool{
        get {
            return UserDefaults.standard.bool(forKey: "IntercomLogin");
        }
        
        set(val) {
            UserDefaults.standard.set(val, forKey: "IntercomLogin")
            UserDefaults.standard.synchronize()
        }
    }
}
