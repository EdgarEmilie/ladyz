//
//  LadyzRadioButton.swift
//  LadyZ
//
//  Created by Admin on 12/20/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class LadyZRadioButton: UIButton {
      
    internal var outerCircleLayer = CAShapeLayer()
    internal var outerSquareLayer = CAShapeLayer()
    internal var innerCircleLayer = CAShapeLayer()
    
    
    @IBInspectable var outerSquareColor: UIColor = UIColor.clear {
        didSet {
            outerSquareLayer.strokeColor = outerSquareColor.cgColor
        }
    }
    @IBInspectable var outerCircleColor: UIColor = UIColor.clear {
        didSet {
            outerCircleLayer.strokeColor = outerCircleColor.cgColor
        }
    }
    @IBInspectable var innerCircleCircleColor: UIColor = UIColor.green {
        didSet {
            setFillState()
        }
    }
    
    @IBInspectable var outerSquareWidth: CGFloat = 3.0 {
        didSet {
            setCircleLayouts()
        }
    }
    @IBInspectable var outerCircleLineWidth: CGFloat = 3.0 {
        didSet {
            setCircleLayouts()
        }
    }
    @IBInspectable var innerCircleGap: CGFloat = 3.0 {
        didSet {
            setCircleLayouts()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInitialization()
    }
    // MARK: Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInitialization()
    }
    
    var setCircleRadius: CGFloat {
        let width = bounds.width - 2*outerSquareWidth
        let height = bounds.height - 2*outerSquareWidth
        
        let length = width > height ? height : width
        return (length - outerCircleLineWidth) / 2
    }
    
    private var setCircleFrame: CGRect {
        let width = bounds.width - 2*outerSquareWidth
        let height = bounds.height - 2*outerSquareWidth
        
        let radius = setCircleRadius
        var x: CGFloat
        var y: CGFloat
        
        if width > height {
            y = outerCircleLineWidth / 2
            x = (width / 2) - radius
        } else {
            x = outerCircleLineWidth / 2
            y = (height / 2) - radius
        }
        x = x + outerSquareWidth
        y = y + outerSquareWidth
        let diameter = 2 * radius
        return CGRect(x: x, y: y, width: diameter, height: diameter)
    }
    
    private var squareCirclePath: UIBezierPath {
        let p = UIBezierPath(roundedRect: setCircleFrame.insetBy(dx: -outerSquareWidth, dy: -outerSquareWidth), cornerRadius: 5)
        let p2 =  UIBezierPath(roundedRect: setCircleFrame.insetBy(dx: -outerCircleLineWidth, dy: -outerCircleLineWidth), cornerRadius: setCircleRadius + outerCircleLineWidth)
        p.append(p2)
        p.usesEvenOddFillRule = true
        return p
    }
    
    private var circlePath: UIBezierPath {
        return UIBezierPath(roundedRect: setCircleFrame, cornerRadius: setCircleRadius)
    }
    
    private var fillCirclePath: UIBezierPath {
        let trueGap = innerCircleGap + (outerCircleLineWidth / 2)
        return UIBezierPath(roundedRect: setCircleFrame.insetBy(dx: trueGap, dy: trueGap), cornerRadius: setCircleRadius)
        
    }
    
    func customInitialization() {
        outerSquareLayer.removeFromSuperlayer()
        outerSquareLayer.frame = bounds
        outerSquareLayer.lineWidth = outerCircleLineWidth
        outerSquareLayer.fillColor = outerSquareColor.cgColor
        outerSquareLayer.strokeColor = outerSquareColor.cgColor
        outerSquareLayer.backgroundColor = UIColor.clear.cgColor
        layer.addSublayer(outerSquareLayer)
        
        outerCircleLayer.removeFromSuperlayer()
        outerCircleLayer.frame = bounds
        outerCircleLayer.lineWidth = outerCircleLineWidth
        outerCircleLayer.fillColor = UIColor.clear.cgColor
        outerCircleLayer.strokeColor = outerCircleColor.cgColor
        outerCircleLayer.backgroundColor = UIColor.clear.cgColor // background
        layer.addSublayer(outerCircleLayer)
        
        innerCircleLayer.removeFromSuperlayer()
        innerCircleLayer.frame = bounds
        innerCircleLayer.lineWidth = outerCircleLineWidth
        innerCircleLayer.fillColor = UIColor.clear.cgColor
        innerCircleLayer.strokeColor = UIColor.clear.cgColor
        innerCircleLayer.backgroundColor = UIColor.clear.cgColor
        layer.addSublayer(innerCircleLayer)
        
        setFillState()
    }
    
    private func setCircleLayouts() {
        outerSquareLayer.frame = bounds
        outerSquareLayer.lineWidth = outerCircleLineWidth
        outerSquareLayer.fillRule = CAShapeLayerFillRule.evenOdd
        outerSquareLayer.path = squareCirclePath.cgPath
        
        outerCircleLayer.frame = bounds
        outerCircleLayer.lineWidth = outerCircleLineWidth
        outerCircleLayer.path = circlePath.cgPath
        
        innerCircleLayer.frame = bounds
        innerCircleLayer.lineWidth = outerCircleLineWidth
        innerCircleLayer.path = fillCirclePath.cgPath
    }
    
    // MARK: Custom
    private func setFillState() {
        if self.isSelected {
            innerCircleLayer.fillColor = innerCircleCircleColor.cgColor
        } else {
            innerCircleLayer.fillColor = UIColor.clear.cgColor
        }
    }
    // Overriden methods.
    override public func prepareForInterfaceBuilder() {
        customInitialization()
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        setCircleLayouts()
    }
    
    override public var isSelected: Bool {
        didSet {
            setFillState()
        }
    }

}
