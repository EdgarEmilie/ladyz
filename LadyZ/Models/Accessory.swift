//
//  Accessory.swift
//  LadieZ
//
//  Created by Admin on 8/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

public struct Accessory {
    var title : String = ""
    var image : [String] = []
    var newPrice : Int? = 0
    var price : Int = 0
    var isFavorite : Bool = false
    
    init(_title : String, _image : [String], _newPrice : Int?, _price : Int , _isfavorite: Bool) {
        image = _image
        newPrice = _newPrice
        price = _price
        isFavorite = _isfavorite
        title = _title
    }
}
