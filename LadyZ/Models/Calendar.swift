//
//  Calendar.swift
//  LadieZ
//
//  Created by Admin on 11/6/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class calendarC : NSObject{
    class func getDays() -> String{
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let dayString = dateFormatter.string(from: date)
        return dayString
    }
    class func getDayWek() -> String{
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E"
        let wekString = dateFormatter.string(from: date)
        return wekString
    }
}
