//
//  Category.swift
//  LadieZ
//
//  Created by Admin on 7/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

public struct Category {
    var image : String = ""
    var title : String = ""
    var tipe : String = ""
    
    init(_image : String , _title : String , _tipe : String) {
        image = _image
        title = _title
        tipe = _tipe
    }
}
