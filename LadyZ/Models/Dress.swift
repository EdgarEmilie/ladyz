//
//  dress.swift
//  Salon
//
//  Created by Admin on 7/15/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

public struct Dress {
    var title : String = ""
    var image : [String] = []
    var price : Int = 0
    var newPrice : Int? = 0
    var isFavorite : Bool = false
    
    init(_title : String, _image : [String], _price : Int, _newPrice : Int?, _isfavorite: Bool) {
        image = _image
        price = _price
        newPrice = _newPrice
        isFavorite = _isfavorite
        title = _title
    }
}
