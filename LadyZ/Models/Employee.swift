//
//  Employee.swift
//  LadieZ
//
//  Created by Admin on 9/24/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

public struct Employee {
    var firstName : String = ""
    var lastName : String = ""
    var image : String = ""
//    var price : Int = 0
//    var isFavorite : Bool = false
    
    init(_firstName : String, _lastName : String,_image : String){//, _price : Int , _isfavorite: Bool) {
        firstName = _firstName
        lastName = _lastName
        image = _image
//        price = _price
//        isFavorite = _isfavorite
    }
}
