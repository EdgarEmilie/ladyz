//
//  ImageSourceView+.swift
//  VR_Ticket
//
//  Created by Georg Balin on 11/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import Imaginary
import ImageIOSwift

extension ImageSourceView {
    func doInMain(_ command: @escaping ()->Void)
    {
        if Thread.isMainThread
        {
            command()
        }
        else{
            DispatchQueue.main.async {
                command()
            }
        }
    }
    func setImageSource(url: URL,
                        placeholder: String? = nil,
                        option: Option? = nil,
                        completion: Completion? = nil) {
        doInMain{
            var placeholderImage = UIImage()
            if placeholder != nil  {
                let imageUrlString = placeholder!
                
               
                if placeholder == "" {
                    placeholderImage = UIImage()
                }else {
                     let imageUrl = URL(string: imageUrlString)!
                    let imageData = try! Data(contentsOf: imageUrl)
                    placeholderImage = UIImage(data: imageData)!
                }
            }
            self.task?.cancel()
            var opt = Option(imageDisplayer: ImageSourceViewDisplayer())
            opt.downloaderMaker = {return ImageDownloader(modifyRequest: {
                self.task = ImageSourceDownloader.shared.download($0, completionHandler: {imageSource, data, res, error in
                })
                let imageSource = self.task?.imageSource
                self.imageSource = imageSource
                return $0
            })}
            self.setImage(url: url, placeholder: placeholderImage/*, placeholder: placeholder*/, option: option ?? opt, completion: completion)
        }
    }
}

