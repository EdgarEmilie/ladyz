//
//  ImageSourceViewDisplayer.swift
//  VR_Ticket
//
//  Created by Georg Balin on 11/10/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import Imaginary
import ImageIOSwift

/// Used to set image onto ImageView
public class ImageSourceViewDisplayer: ImageDisplayer {
    
    private let animationOption: UIView.AnimationOptions
    
    public init(animationOption: UIView.AnimationOptions = .transitionCrossDissolve) {
        self.animationOption = animationOption
    }
    
    public func display(placeholder: Image, onto view: View) {
        guard let imageView = view as? ImageSourceView else {
            return
        }
        
        if let data = placeholder.pngData() {
            let imageSource = ImageSource(data: data)
            imageView.imageSource = imageSource
        }
        //        imageView.image = placeholder
    }
    
    public func display(image: Image, onto view: View) {
        guard let imageView = view as? ImageSourceView else {
            return
        }
        
        UIView.transition(with: imageView, duration: 0.25,
                          options: [self.animationOption, .allowUserInteraction],
                          animations: {
                            if let data = image.pngData() {
                                let imageSource = ImageSource(data: data)
                                imageView.imageSource = imageSource
                            }
                            //                            imageView.image = image
        }, completion: nil)
    }
}
