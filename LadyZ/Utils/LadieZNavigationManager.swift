//
//  VRNavigationManager.swift
//  LadieZ
//
//  Created by coder on 7/31/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

class LadieZNavigationManager
{
    static let instance = LadieZNavigationManager()
    var dress: Dress?
    var pg: Int?
    enum Page: Int
    {
        case Default = 0
        case Home = 1
        case Dress = 2
        case PrivacyPolicy = 3
        case Favorites = 4
        case Language = 5
        case Setting = 6
        case Help = 7
        case LogOut = 8
        case Notifications = 9
        case Login = 10
        case promo = 11
        case Version = 12
        case Acces = 13
        case Employees = 14
        case Cart = 15
        case Bonus = 16
    }
    
    
    func open(_ page: Page, _ param: Any? = nil, _ param2: Any? = nil)
    {
        switch page {
        case .Employees:
            guard let employee: Employee = param as? Employee else {
                return
            }
            openEmployee(employee)
            break
        case .Dress:
            guard let dress: Dress = param as? Dress else {
                return
            }
            openDress(dress)
            break
        case .Acces:
            guard let acces: Accessory = param as? Accessory else {
                return
            }
            openAcces(acces)
            break
        case .Home:
            openHome()
            break
        case .Notifications:
            //notification()
            print("Notification")
            break
        case .Favorites:
            //openFavorites()
            break
        case .Language:
            openLanguage()
            break
        case .Setting:
            //openSettings()
            break
        case .Login:
            //openLogin()
            break
        case .Help:
            //openHelp(url: "help-page")
            break
        case .PrivacyPolicy:
            //openHelp(url: "privacy-policy")
            break
        case .LogOut:
            //logOut()
            break
        case .promo:
            guard let dress: Dress = param as? Dress else {
                return
            }
            //openPromo(dress,param2 as! String)
        case .Cart:
            openCart()
            print("Cart")
            break
        case .Bonus:
            openBonus()
            break
        case . Version:
            print("version")
            break
        default:
            openHome()
        }
    }
    
    func openHome()
    {
        doInMain {
            if let _ = UIApplication.shared.topMostViewController() {} else {
                self.openHomeIfNeeded(topmostViewController: nil)
            }
            guard let topController = UIApplication.shared.topMostViewController() else {
                return
            }
            
//            guard let rev = topController.revealViewController() else {
//                return
//            }
    //        let storyboard = UIStoryboard(name: "Reveal", bundle: nil)
    //        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeNavVC") as! UINavigationController
            topController.navigationController?.popToRootViewController(animated: false)
            
            //rev.rightRevealToggle(animated: true)
            print("giong home")
        }
    }
    
    func openMyTickets()
    {
        doInMain {
            guard let topController = UIApplication.shared.topMostViewController() else {
                return
            }
            let storyboard = UIStoryboard(name: "Reveal", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyTicketsVC")
            topController.navigationController?.pushViewController(vc, animated: false)
    //        rev.pushFrontViewController(nil, animated: true)
//            rev.rightRevealToggle(animated: true)
            print("giong myTickets")
        }
    }
    func set(){
       
    }
    
    
    func doInMain(_ command: @escaping ()->Void)
    {
        if Thread.isMainThread
        {
            command()
        }
        else{
            DispatchQueue.main.async {
                command()
            }
        }
    }
    
    func openHomeIfNeeded(topmostViewController: UIViewController?) {
        doInMain {
            if let vc = topmostViewController {
                if let viewControllers = vc.navigationController?.viewControllers {
                    var createHome = false
                    if(viewControllers.count > 0) {
                        if let _: HomeViewController = viewControllers[0] as? HomeViewController {
                        //}else if let _: SalonCatTableViewController = viewControllers[0] as? SalonCatTableViewController {
                        }else if let _: DressCatTableViewController = viewControllers[0] as? DressCatTableViewController {
                        }else if let _: AccessoriesCatTableViewController = viewControllers[0] as? AccessoriesCatTableViewController {
                        }else if let _: AccountViewController = viewControllers[0] as? AccountViewController {
                        } else {
                            createHome = true
                        }
                    } else {
                        createHome = true
                    }
                    if(createHome) {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let tabBar = storyboard.instantiateViewController(withIdentifier: "HomeVC")
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = tabBar
                        
                        if let homeVC = vc.storyboard?.instantiateViewController(withIdentifier: "HomeVC") {
                            vc.navigationController?.pushViewController(homeVC, animated: false)
                        }
                    }
                }
            } else {
                    if let app = UIApplication.shared.delegate as? AppDelegate
                    {
                        app.createHome()
                    }
            }
        }
    }
    func openEmployeeCollection(_ param : String)
    {
        doInMain {
            if let vc = UIApplication.shared.topMostViewController() {
                let employeeVC = vc.storyboard?.instantiateViewController(withIdentifier: "employeeCollectionVC") as! EmployeesCollectionViewController
                employeeVC.param = param
                self.openHomeIfNeeded(topmostViewController: vc)
                vc.navigationController?.pushViewController(employeeVC, animated: true)
            } else {
                self.openHomeIfNeeded(topmostViewController: nil)
                self.openEmployeeCollection(param)
                return
            }
        }
    }
    
    func openEmployee(_ employee : Employee)
    {
        doInMain {
            if let vc = UIApplication.shared.topMostViewController() {
                let employeeVC = vc.storyboard?.instantiateViewController(withIdentifier: "employeeVC") as! EmployeeViewController
                employeeVC.employee = employee
                self.openHomeIfNeeded(topmostViewController: vc)
                vc.navigationController?.pushViewController(employeeVC, animated: true)
            } else {
                self.openHomeIfNeeded(topmostViewController: nil)
                self.openEmployee(employee)
                return
            }
        }
    }
    func openDressCollection(_ param : String)
    {
        doInMain {
            if let vc = UIApplication.shared.topMostViewController() {
                let dressVC = vc.storyboard?.instantiateViewController(withIdentifier: "dressCollectionVC") as! DressCollectionViewController
                dressVC.param = param
                self.openHomeIfNeeded(topmostViewController: vc)
                vc.navigationController?.pushViewController(dressVC, animated: true)
            } else {
                self.openHomeIfNeeded(topmostViewController: nil)
                self.openEmployeeCollection(param)
                return
            }
        }
    }
    func openDress(_ dress: Dress)
    {
        doInMain {
            if let vc = UIApplication.shared.topMostViewController() {
                let dressVC = vc.storyboard?.instantiateViewController(withIdentifier: "DressVC") as! DressViewController
                dressVC.dress = dress
                self.openHomeIfNeeded(topmostViewController: vc)
                vc.navigationController?.pushViewController(dressVC, animated: true)
            } else {
                self.openHomeIfNeeded(topmostViewController: nil)
                self.openDress(dress)
                return
            }
        }
    }
    func openAccesCollection(_ param : String)
    {
        doInMain {
            if let vc = UIApplication.shared.topMostViewController() {
                let AccesVC = vc.storyboard?.instantiateViewController(withIdentifier: "accessCollectionVC") as! AccessoriesCollectionViewController
                AccesVC.param = param
                self.openHomeIfNeeded(topmostViewController: vc)
                vc.navigationController?.pushViewController(AccesVC, animated: true)
            } else {
                self.openHomeIfNeeded(topmostViewController: nil)
                self.openEmployeeCollection(param)
                return
            }
        }
    }
    func openAcces(_ acces: Accessory)
    {
        doInMain {
            if let vc = UIApplication.shared.topMostViewController() {
                let dressVC = vc.storyboard?.instantiateViewController(withIdentifier: "AccesVC") as! AccessoryViewController
                dressVC.acces = acces
                self.openHomeIfNeeded(topmostViewController: vc)
                vc.navigationController?.pushViewController(dressVC, animated: true)
            } else {
                self.openHomeIfNeeded(topmostViewController: nil)
                self.openAcces(acces)
                return
            }
        }
    }
    func openCart(){
        doInMain {
            guard let topController = UIApplication.shared.topMostViewController() else {
                return
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CartVC")
            topController.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    func openBonus(){
        doInMain {
            guard let topController = UIApplication.shared.topMostViewController() else {
                return
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "BonusVC")
            topController.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func notification() {
        doInMain {
            guard let topController = UIApplication.shared.topMostViewController() else {
                return
            }
            let alert = UIAlertController (title: "", message: Swifternalization.localizedString("go_to_settings_message"), preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: Swifternalization.localizedString("settings"), style: UIAlertAction.Style.cancel) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
            alert.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: Swifternalization.localizedString("cancel"), style: .default, handler: nil)
            alert.addAction(cancelAction)
            alert.view.tintColor = colour.buttonColor
            topController.present(alert, animated: true, completion: nil)
        }
    }
    func openLanguage(){
    doInMain {
        guard let topController = UIApplication.shared.topMostViewController() else {
            return
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "languageVC")
        topController.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func openHelp(url: String) {
    doInMain {
        guard let topController = UIApplication.shared.topMostViewController() else {
            return
        }
        let storyboard = UIStoryboard(name: "Reveal", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WebVC") //as! WebViewController
        //vc.url = url
        topController.navigationController?.pushViewController(vc, animated: true)
        
        }
    }
    func openSettings(){
    doInMain {
            guard let topController = UIApplication.shared.topMostViewController() else {
                return
            }
//            guard let rev = topController.revealViewController() else {
//                return
//            }
            let storyboard = UIStoryboard(name: "Reveal", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SettingsVC")
            topController.navigationController?.pushViewController(vc, animated: true)
            //        rev.pushFrontViewController(nil, animated: true)
//            rev.rightRevealToggle(animated: true)
        }
    }
    func openFavorites() {
        doInMain {
            guard let topController = UIApplication.shared.topMostViewController() else {
                return
            }
//            guard let rev = topController.revealViewController() else {
//                return
//            }
            let storyboard = UIStoryboard(name: "Reveal", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FavouritesVC")
            topController.navigationController?.pushViewController(vc, animated: false)
            //        rev.pushFrontViewController(nil, animated: true)
//            rev.rightRevealToggle(animated: true)
        }
    }
    func openLogin() {
        doInMain { //keyWindow?.parentViewController
            guard let vc = UIApplication.shared.topMostViewController()  else {
                return
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc1 = storyboard.instantiateViewController(withIdentifier: "LoginVC") //as! LoginViewController
            vc.navigationController?.pushViewController(vc1, animated: false)
            
        }
    }
    func logOut() {
        doInMain {
            if let a = UIApplication.shared.topMostViewController() {
                //a.revealViewController()?.revealToggle(animated: true)
            }
            //VRUser.accessToken = ""
        }
    }
//    func openPromo(_ event: Event,_ text : String)
//    {
//        doInMain {
//            if let vc = UIApplication.shared.topMostViewController() {
//                print(vc.storyboard?.instantiateViewController(withIdentifier: "PromoVC") as! PromoViewController)
//                let promoVC = vc.storyboard?.instantiateViewController(withIdentifier: "PromoVC") as! PromoViewController
//                promoVC.event = event
//                promoVC.text = text
//                self.openHomeIfNeeded(topmostViewController: vc)
//                vc.navigationController?.pushViewController(promoVC, animated: true)
//            } else {
//                self.openHomeIfNeeded(topmostViewController: nil)
//                self.openEvent(event)
//                return
//            }
//        }
//    }
    
    
    ///OLD
//            guard let topController = UIApplication.shared.topMostViewController() else {
//                return
//            }
//            if let checkoutVC = topController.storyboard?.instantiateViewController(withIdentifier: "CheckoutVC") as? CheckoutViewController
//            {
//                topController.navigationController?.viewControllers.insert(checkoutVC, at: (topController.navigationController?.viewControllers.endIndex)! - 1)
//            }
//            topController.navigationController?.popViewController(animated: true)
    
//    func openHelp()
//    {
//        guard let topController = UIApplication.shared.topMostViewController() else {
//            return
//        }
//        guard let rev = topController.revealViewController() else {
//            return
//        }
//        let storyboard = UIStoryboard(name: "Reveal", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "HelpVC")
//        topController.navigationController?.pushViewController(vc, animated: false)
//        //        rev.pushFrontViewController(nil, animated: true)
//        rev.rightRevealToggle(animated: true)
//    }
    func back()
    {
//        if self.pg == 2 {
//            LadieZNavigationManager.instance.openHome()
//            TabBarController.instance?.selectedIndex = 1
//            pg = nil
//        }
//        if self.event != nil {
//            VRNavigationManager.instance.openHome()
//            VRNavigationManager.instance.openEvent(event!)
//            if pg == 1 { // :TODO
//                BuyController.startOrder(event!)
//            }else  {
//            }
//            pg = nil
//            self.event = nil
//        } else {
            if let topController = UIApplication.shared.topMostViewController() {
                topController.navigationController?.popViewController(animated: true)
            }
        //}
        
    }
}
