
//
//  NibNameLoadable.swift
//  LadieZ
//
//  Created by coder on 7/30/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

public protocol NibNameLoadable: class {
    
    var contentView:UIView? { get set }
    var nibName:String? { get set }
    
    func setupContentView()
    
}

public extension NibNameLoadable where Self: UIView  {
    func xibSetup() {
        if contentView != nil { return }
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        setupContentView()
    }
    
    private func loadViewFromNib() -> UIView? {
        guard let nibName = nibName else { return nil }
        self.nibName = nil
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        contentView = nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
        return contentView
    }
}

