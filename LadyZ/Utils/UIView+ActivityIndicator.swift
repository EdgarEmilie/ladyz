//
//  UIView+ActivityIndicator.swift
//  LadieZ
//
//  Created by coder on 8/21/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

extension UIView {
    
    private func doInMain(_ command: @escaping ()->Void)
    {
        if Thread.isMainThread
        {
            command()
        }
        else{
            DispatchQueue.main.async {
                command()
            }
        }
    }
    
    func StartAnimaiting(frame: CGRect? = nil, color: UIColor? = UIColor.white) {
        doInMain{
            self.StopAnimating()
            var a: NVActivityIndicatorView? = nil
            if frame == nil {
                a = self.setup( Activity: NVActivityIndicatorView(frame: CGRect(x: self.frame.maxX - 50 , y: self.frame.maxY - 80, width: 30, height: 30), type: .lineScalePulseOut, color: color), fors: true)
            }
            else {
                a = self.setup( Activity: NVActivityIndicatorView(frame: frame!, type: .lineScalePulseOut, color: color), fors: true)
            }
            self.addSubview(a!)
            
            self.bringSubviewToFront(a!)
            a?.startAnimating()
        }
    }
    
    func StartPlayerAnimaiting(frame: CGRect? = nil) {
        doInMain {
            self.StopAnimating()
            var a: NVActivityIndicatorView? = nil
            if frame == nil {
                a = self.setup( Activity: NVActivityIndicatorView(frame: CGRect(x: self.center.x , y: self.center.y, width: self.frame.width, height: self.frame.height), type: .ballScaleMultiple, color: UIColor.white), fors: true)
            }
            else {
                a = self.setup( Activity: NVActivityIndicatorView(frame: frame!, type: .ballScaleRippleMultiple, color: UIColor.white), fors: true)
            }
            self.addSubview(a!)
            a?.startAnimating()
        }
    }
    
    func StopAnimating() {
        doInMain {
            let a = self.setup()
                a?.stopAnimating()
                a?.isHidden = true
                a?.removeFromSuperview()
                let _ = self.setup(Activity: nil, fors: true)
           
        }
    }
    
    private func setup (Activity : NVActivityIndicatorView? = nil,fors:Bool = false) -> NVActivityIndicatorView? {
        struct Setup {
            static var activityIndicator : NVActivityIndicatorView?
            
        }
        if Activity != nil || fors == true {
            Setup.activityIndicator = Activity
        }
        return Setup.activityIndicator
    }
    
}
