//
//  UIView+AlertViewController.swift
//  LadieZ
//
//  Created by Admin on 11/14/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    private func doInMain(_ command: @escaping ()->Void)
    {
        if Thread.isMainThread
        {
            command()
        }
        else{
            DispatchQueue.main.async {
                command()
            }
        }
    }
    
    func errorAlert(errorTitle: String , errorMessage: String) -> UIAlertController{
        //doInMain {
        let alert = UIAlertController.init(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: Swifternalization.localizedString("ok"), style: .default, handler: {(nil) in
            //Add callBack
        }))
        alert.view.tintColor = colour.buttonColor
        
        alert.view.tintColor = UIColor(red:0.40, green:0.37, blue:1.00, alpha:1.0)
        return alert
        //}
    }
}

class ErrorReporting {
    
    static func showMessage(title: String, messsage: String) {
        let alert = UIAlertController(title: title, message: messsage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Swifternalization.localizedString("ok"), style: UIAlertAction.Style.default, handler: nil))
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        alert.view.tintColor = colour.buttonColor
    }
}
extension UIApplication {
    
    static func topViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return topViewController(base: selected)
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}
