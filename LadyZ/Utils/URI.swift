//
//  URI.swift
//  VR_Ticket
//
//  Created by Admin on 12/8/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
class VRURI {
    private init(){
        
    }
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
}
