//
//  AccessoriesTableViewController.swift
//  LadieZ
//
//  Created by Admin on 7/19/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class AccessoriesCatTableViewController: UITableViewController {

    var cat : [Category] = [Category(_image: "acess2", _title: "bracers", _tipe: "bracer"),Category(_image: "acess3", _title: "rings", _tipe: "rings")]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "ACCESSORIES"
        tableView.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        // self.clearsSelectionOnViewWillAppear = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = colour.backgraundColor
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    // MARK: - Table view data source


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.isNavigationBarHidden = true
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return cat.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CategoryTableViewCell
        cell.category = cat[indexPath.row]
        cell.backgroundColor = colour.backgraundColor
        // Configure the cell...
        cell.selectionStyle = .none
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LadieZNavigationManager.instance.openAccesCollection(cat[indexPath.row].tipe)

    }

}
