//
//  AccessoriesCollectionViewController.swift
//  LadieZ
//
//  Created by Admin on 8/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class AccessoriesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    private var accessory: [Accessory] = [Accessory.init(_title: "Bracer", _image: ["acess1","dress1"], _newPrice: 2000, _price : 3000, _isfavorite: false),Accessory.init(_title: "Bracer",_image: ["acess2","acess2"], _newPrice: nil, _price : 4000, _isfavorite: false),Accessory.init(_title: "Bracer",_image: ["acess1","acess1"], _newPrice: nil, _price : 3000, _isfavorite: false),Accessory.init(_title: "Bracer",_image: ["acess2"], _newPrice: 2600, _price : 3000, _isfavorite: false)]
    private var accessory1: [Accessory] = [Accessory.init(_title: "Rings", _image: ["acess1","dress1"], _newPrice: 2000, _price : 3000, _isfavorite: false),Accessory.init(_title: "Rings",_image: ["acess2","acess2"], _newPrice: nil, _price : 4000, _isfavorite: false)]
    var param : String? {
        didSet {
            guard let _param : String = self.param else{
                return
            }
            set(_param)
        }
    }
    func set(_ param : String){
        if param == "0" {
            
        }else if param == "1"{
            
        }else if param == "2"{
            
        }
    }
    struct Storyboard {
        static let cell = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.backgroundColor = colour.backgraundColor
        self.collectionView!.register(UINib(nibName: "AccessoriesCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        setupNavigationController()
        // Animate up to statusbar.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.isNavigationBarHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 2 - 14
        return CGSize(width: width , height: width  + 41 )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if param == "bracer"{
            return accessory.count
        }else if param == "rings"{
            return accessory1.count
        }
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? AccessoriesCell else {
            return UICollectionViewCell()
        }
        //        cell.layer.cornerRadius = 10
        //        cell.layer.masksToBounds = true
        cell.backgroundColor = .white
        if param == "bracer"{
            cell.accessory = accessory[indexPath.row]
        }else if param == "rings"{
            cell.accessory = accessory1[indexPath.row]
        }
        
        
        return cell
    }
    
   
    
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        LadieZNavigationManager.instance.openAcces(accessory[indexPath.row])
        return true
    }
    
    func setupNavigationController() {
        if param == "bracer"{
            self.navigationItem.title = "BRACERS"
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Bracers", style: .plain, target: nil, action: nil)
        }else if param == "rings"{
            self.navigationItem.title = "RINGS"
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Rings", style: .plain, target: nil, action: nil)
        }
    }

}
