//
//  AccessoryViewController.swift
//  LadieZ
//
//  Created by Admin on 8/28/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AccessoryViewController: UIViewController {

    var acces : Accessory? {
        didSet {
            guard let _acces : Accessory = self.acces else{
                return
            }
            set (acces : _acces)
        }
    }
    func set (acces : Accessory) {
        self.view.layoutIfNeeded()
        guard let _ = view else {
            print("viewNOT!")
            return
        }
        //title.text = String(dress.title)
        //ReserveButton.setTitle(String(dress.price) + "AMD", for: .normal)
        
        if acces.newPrice != nil {
            topPrice.text = String(acces.newPrice!) + "AMD"
            topPrice.textColor = colour.buttonColor
            bottomPrice.text = String(acces.price) + "AMD"
        }else {
            topPrice.text = String(acces.price) + "AMD"
            bottomPrice.isHidden = true
        }
    }
    
    @IBOutlet weak var topPrice: UILabel!
    @IBOutlet weak var bottomPrice: strikethroughLabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var ReserveButton: UIButton!
    @IBOutlet weak var Description: UILabel!
    //@IBOutlet weak var rating: CosmosView!
    
    
    @IBAction func addToCart(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ReserveButton.layer.cornerRadius = 5
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        scrollView.backgroundColor = colour.backgraundColor
        scrollView.delegate = self
        Description.font = UIFont.LadyzFont(name: .RobotoLight, size: 15)
        //ReserveButton.setGradient(from: UIColor(red: 0.91, green: 0.25, blue: 0.58, alpha: 1), to: UIColor(red: 0.96, green: 0.31, blue: 0.37, alpha: 1))
        ReserveButton.setImage(UIImage(named: "add_cart"), for: .normal)
        
//        if #available(iOS 11.0, *) {
//            let a = UIApplication.shared.statusBarFrame.size.height
//            let b = navigationController?.navigationBar.frame.height ?? 44
//            scrollView.contentInset.top = -b//(a + b)
//        }else {
//            
//        }
    }
    
}
extension AccessoryViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let size = collectionView.frame
        let imageview:UIImageView=UIImageView(frame: CGRect(x: size.minX, y: size.minY, width: size.size.width, height: size.size.height));

        let img : UIImage = UIImage(named:String((acces?.image[indexPath.row])!))!
            imageview.image = img

            cell.contentView.addSubview(imageview)

        return cell
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ImageCollectionViewCell
//        cell?.imageView.image = UIImage(named: String((acces?.image[indexPath.row])!))
//        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if acces?.image.count == 1 {
            pageControl.isHidden = true
        }else{
            pageControl.numberOfPages = acces?.image.count ?? 0
        }
        return acces?.image.count ?? 0
    }
}

extension AccessoryViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
