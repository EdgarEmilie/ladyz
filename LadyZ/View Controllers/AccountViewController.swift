//
//  AccountViewController.swift
//  LadieZ
//
//  Created by Admin on 7/19/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userInfoView: UIView!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var LoginButton: MainButton!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var Letter: UILabel!
    
    @IBOutlet var MenuCells: [MenuCellHolder]!
    @IBOutlet weak var logOut: MenuCellHolder!
    @IBOutlet weak var logOutHeight: NSLayoutConstraint!
    @IBOutlet var Sections: [UILabel]!
    static var instance : AccountViewController = AccountViewController()
    
    let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AccountViewController.instance = self
        topView.backgroundColor = colour.backgraundColor
        scrollView.backgroundColor = colour.backgraundColor
        Sections[0].superview?.backgroundColor = colour.buttonColor.withAlphaComponent(0.3)
        Sections[1].superview?.backgroundColor = colour.buttonColor.withAlphaComponent(0.3)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        setTexts()
        navigationController?.isNavigationBarHidden = true
    }
    // TODO delete this
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    func setTexts() {
        if MenuCells != nil {
            for i in 0..<MenuCells.count {
                //            if MenuCells[i].tag <= MenuCells.count{
                cellsName(cell:MenuCells[i])
                //            }
            }
            Sections[0].font = UIFont.LadyzFont(name: .RobotoMedium, size: 16)
            Sections[1].font = UIFont.LadyzFont(name: .RobotoMedium, size: 16)
            Sections[0].text = Swifternalization.localizedString("general")
            Sections[1].text = Swifternalization.localizedString("about")
        }
        userInfoView.isHidden = true
    }
    
    private func cellsName (cell : MenuCellHolder ) {
        typealias navi = LadieZNavigationManager.Page
        guard let O = navi(rawValue: cell.tag) else {return}
        switch O {
        case navi.Notifications:
            cell.text = Swifternalization.localizedString("notification")
            cell.icon = ("notification")
            let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
            if isRegisteredForRemoteNotifications {
                cell.image = ("on")
            } else {
                cell.image = ("off")
            }
            break
        case navi.Language:
            cell.text = Swifternalization.localizedString("language")
            cell.icon = ("language")
            cell.image = ("chevron_right")
            if(Globals.sharedInstance.selectedLanguage == "en"){
                cell.text2 = "English"  }
            else if(Globals.sharedInstance.selectedLanguage == "ru"){
                cell.text2 = "Русский"
            }else if(Globals.sharedInstance.selectedLanguage == "am"){
                cell.text2 = "Hayeren"
            }
            break
        case navi.Cart:
            cell.text = "Cart"
            cell.icon = ("cart")
            cell.badgeText = "2"
            cell.image = ("chevron_right")
            break
        case navi.Help:
            cell.text = Swifternalization.localizedString("help")
            cell.icon = ("help")
            cell.image = ("chevron_right")
            break
        case navi.Favorites:
            cell.text = Swifternalization.localizedString("favorite")
            cell.icon = ("star")
            break
        case navi.Setting:
            cell.text = Swifternalization.localizedString("settings")
            cell.icon = ("settings")
            break
        case navi.PrivacyPolicy:
            cell.text = Swifternalization.localizedString("privacy_policy")
            cell.icon = ("privacy_policy")
            cell.image = ("chevron_right")
            break
        case navi.Version:
            cell.text = Swifternalization.localizedString("version")
            cell.icon = ("version")
//            if version != nil {
//                cell.text2 = version!
//            }
            cell.text2 = version ?? ""
            break
        case navi.Bonus:
            cell.text = Swifternalization.localizedString("Bonus")
            cell.icon = ("bonus")
            cell.badgeText = "1"
            cell.image = ("chevron_right")
        case navi.LogOut:
            cell.text = Swifternalization.localizedString("logout")
            cell.icon = ("logout")
            break
        case navi.Login:
            cell.text = Swifternalization.localizedString("login")
            break
        default:
            break
            
        }
        
    }
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag)
    }
    
}
