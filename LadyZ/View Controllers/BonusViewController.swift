//
//  BonusViewController.swift
//  LadyZ
//
//  Created by Admin on 12/27/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class BonusViewController: UIViewController {
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bonusView: UIView!
    @IBOutlet weak var bonus: UILabel!
    @IBOutlet weak var bonusVal: UILabel!
    
    
    static var instance : BonusViewController = BonusViewController()
    
    let text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BonusViewController.instance = self
        navigationItem.title = Swifternalization.localizedString("Bonus")
        if self.textView.attributedText != nil {
            let a = text.htmlToAttributedString
            self.textView.attributedText = a
            self.textView.font = UIFont.LadyzFont(name: .RobotoLight, size: 14)
        }
        bonus.font = UIFont.LadyzFont(name: .RobotoRegular, size: 16)
        bonus.text = Swifternalization.localizedString("Bonus")
        bonusVal.font = UIFont.LadyzFont(name: .RobotoRegular, size: 16)
        bonusVal.text = Swifternalization.localizedString("2300 AMD")
        
        infoView.backgroundColor = colour.backgraundColor
        view.backgroundColor = colour.backgraundColor
        tableView.separatorStyle = .none
        tableView.backgroundColor = colour.backgraundColor
        navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    
    @IBAction func moreInfo(_ sender: Any) {
    }
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
