//
//  CartTableViewController.swift
//  LadyZ
//
//  Created by Admin on 12/19/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
private let reuseIdentifier = "Cell"
class CartTableViewController: UITableViewController{
    
    var access : [Accessory] = [Accessory.init(_title: "Rings", _image: ["acess1","dress1"], _newPrice: 2000, _price : 3000, _isfavorite: false),Accessory.init(_title: "Rings",_image: ["acess2","acess2"], _newPrice: nil, _price : 4000, _isfavorite: false)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
       
        self.navigationItem.title = "Cart"
        tableView.register(UINib(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        // self.clearsSelectionOnViewWillAppear = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = colour.backgraundColor
    }
    
    // MARK: - Table view data source
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.isNavigationBarHidden = true
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return access.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 10
//    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CartTableViewCell
        cell.accessory = access[indexPath.row]
        // Configure the cell...
        cell.backgroundColor = colour.backgraundColor
        cell.selectionStyle = .none
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //LadieZNavigationManager.instance.openDressCollection(access[indexPath.row].tipe)
    }
    
}
