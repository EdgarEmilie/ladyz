//
//  DressCatTableViewController.swift
//  LadyZ
//
//  Created by Admin on 11/12/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
private let reuseIdentifier = "Cell"

class DressCatTableViewController: UITableViewController {
    var cat : [Category] = [Category(_image: "EVDressIcon", _title: "Evening Dresses", _tipe: "evDress"),Category(_image: "WdressIcon", _title: "Wedding Dresses", _tipe: "wDress")]
    
       override func viewDidLoad() {
           
            super.viewDidLoad()
            self.navigationItem.title = "DRESSES"
           tableView.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
           // self.clearsSelectionOnViewWillAppear = false
           tableView.separatorStyle = .none
           tableView.backgroundColor = colour.backgraundColor
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
       }
       
       // MARK: - Table view data source
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           //navigationController?.isNavigationBarHidden = true
       }
       override func numberOfSections(in tableView: UITableView) -> Int {
           // #warning Incomplete implementation, return the number of sections
           return 1
       }
       
       override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           // #warning Incomplete implementation, return the number of rows
           return cat.count
       }
       override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 120
       }
       
       override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CategoryTableViewCell
           cell.category = cat[indexPath.row]
           // Configure the cell...
           cell.backgroundColor = colour.backgraundColor
           cell.selectionStyle = .none
           return cell
       }
       override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LadieZNavigationManager.instance.openDressCollection(cat[indexPath.row].tipe)
       }
}
