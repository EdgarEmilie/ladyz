//
//  DressCollectionViewController.swift
//  LadieZ
//
//  Created by Admin on 7/15/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
private let reuseIdentifier1 = "Cell"

class DressCollectionViewController: UICollectionViewController ,UICollectionViewDelegateFlowLayout{
    private var dress: [Dress] = [Dress.init(_title: "TITLE", _image: ["dress1","dress1"], _price: 10000, _newPrice: nil, _isfavorite: false),Dress.init(_title: "TITLE4",_image: ["dress2","dress2"], _price: 12000, _newPrice: nil, _isfavorite: false),Dress.init(_title: "TITLE",_image: ["dress3","dress3"], _price: 10000, _newPrice: nil, _isfavorite: false),Dress.init(_title: "TITLE2",_image: ["dress4"], _price: 16000, _newPrice: nil, _isfavorite: false),Dress.init(_title: "TITLE",_image: ["dress5","dress5"], _price: 10000, _newPrice: nil,_isfavorite: false),Dress.init(_title: "TITLE1",_image: ["dress4"], _price: 16000, _newPrice: nil, _isfavorite: false),Dress.init(_title: "TITLE",_image: ["dress6","dress6"], _price: 10000, _newPrice: nil,_isfavorite: false)]
    private var wDress: [Dress] = [Dress.init(_title: "TITLE", _image: ["dressSV1","dressSV1"], _price: 100000, _newPrice: nil, _isfavorite: false)]
    var param : String? {
        didSet {
            guard let _param : String = self.param else{
                return
            }
            set(_param)
        }
    }
    func set(_ param : String){
        if param == "0" {
            
        }else if param == "1"{
            
        }else if param == "2"{
            
        }
    }
    struct Storyboard {
        static let cell = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.backgroundColor = colour.backgraundColor
        self.collectionView!.register(UINib(nibName: "DressCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier1)
        setupNavigationController()
        // Animate up to statusbar.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //navigationController?.isNavigationBarHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 2 - 14
        return CGSize(width: width , height: width * 7 / 5 + 50 )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if param == "evDress" {
            return dress.count
        }else if param == "wDress" {
            return wDress.count
        }
        return 1
    }
    

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier1, for: indexPath) as? DressCell else {
            return UICollectionViewCell()
        }
//        cell.layer.cornerRadius = 10
//        cell.layer.masksToBounds = true
        //cell.backgroundColor = .clear
        if param == "evDress" {
            cell.dress = dress[indexPath.row]
        }else if param == "wDress" {
            cell.dress = wDress[indexPath.row]
        }
        return cell
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        LadieZNavigationManager.instance.openDress(dress[indexPath.row])
        return true
    }
 
    func setupNavigationController() {
        if param == "evDress" {
            self.navigationItem.title = "EVENING DRESSES"
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Evneing dresses", style: .plain, target: nil, action: nil)
        }else if param == "wDress" {
            self.navigationItem.title = "WEDDING DRESSES"
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Wedding Dresses", style: .plain, target: nil, action: nil)
        }
        
    }

}
