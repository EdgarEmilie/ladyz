//
//  DressViewController.swift
//  Salon
//
//  Created by Admin on 7/15/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Cosmos

class DressViewController: CalendarViewController {
    var dress : Dress? {
        didSet {
            guard let _dress : Dress = self.dress else{
                return
            }
            set (dress : _dress)
        }
    }
    func set (dress : Dress) {
        self.view.layoutIfNeeded()
        guard let _ = view else {
            print("viewNOT!")
            return
        }
        Dresstitle.text = String(dress.title)
        ReserveButton.setTitle("Reserve", for: .normal)
        //ReserveButton.setTitle(String(dress.price) + "AMD", for: .normal)
    }
    @IBOutlet weak var Dresstitle: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var ReserveButton: MainButton!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var Description: UILabel!
    //@IBOutlet weak var CalendarView: CalenderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        scrollView.delegate = self
        scrollView.backgroundColor = colour.backgraundColor
        Description.font = UIFont.LadyzFont(name: .RobotoLight, size: 15)//UIFont(name: "Roboto-Light", size: 15)
        Description.text = "DescriptionMeasureReturn PolicyFAQs\nSpecificationsProduct Name: Ericdress Cap Sleeve Mid-Calf Falbala Plain Pullover DressItem Code: 14204292Gross Weight/Package:  0.502( kg )\nMaterial:PolyesterSilhouette:AsymmetricalHemline:Mid-CalfSleeve Length:Cap SleeveCombination Type:SingleWaist Line:Standard-WaistClosure:PulloverElasticity:InelasticDetachable Collar:NoPattern:PlainEmbellishments:Asymmetric,Ruffles,Patchwork,Split Size Details"
        
        // if you want scroll view to be started to top status bar
//        if #available(iOS 11.0, *) {
//            let a = UIApplication.shared.statusBarFrame.size.height
//            let b = navigationController?.navigationBar.frame.height ?? 44
//            scrollView.contentInset.top = -b//(a + b)
//        }else {
//            
//        }
        installCalendar()
    }
    func installCalendar() {
        calenderView.myCollectionView.collectionViewLayout.invalidateLayout()
//        view.addSubview(onConfirmeButton)
        scrollView.addSubview(calenderView)

        let h = CGFloat(300)//calenderView.myCollectionView.frame.height + 200
        calenderView.topAnchor.constraint(equalTo: Description.bottomAnchor, constant: 10).isActive=true
        calenderView.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: -12).isActive=true
        calenderView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 12).isActive=true
        //calenderView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -45).isActive=true
        calenderView.heightAnchor.constraint(equalToConstant: h).isActive=true

        //onConfirmeButton.topAnchor.constraint(equalTo: calenderView.bottomAnchor, constant: 0).isActive=true
//        onConfirmeButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive=true
//        onConfirmeButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 12).isActive=true
//        onConfirmeButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -12).isActive=true
//        onConfirmeButton.heightAnchor.constraint(equalToConstant: 35).isActive=true
       // calenderView.setTime()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        
        DispatchQueue.main.async {
            var contentRect = CGRect.zero

            for view in self.scrollView.subviews {
               contentRect = contentRect.union(view.frame)
            }

            self.scrollView.contentSize = CGSize(width: 0, height: contentRect.size.height + 50)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    @IBAction func Reserve(_ sender: Any) {
        let bottomOffset = CGPoint(x: 0, y: calenderView.frame.minY - 200)
        scrollView.setContentOffset(bottomOffset, animated: true)
    }
    
}

extension DressViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let size = collectionView.frame
        let imageview:UIImageView=UIImageView(frame: CGRect(x: size.minX, y: size.minY, width: size.size.width, height: size.size.height));

        let img : UIImage = UIImage(named:String((dress?.image[indexPath.row])!))!
            imageview.image = img

            cell.contentView.addSubview(imageview)

        return cell
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ImageCollectionViewCell
//        cell?.imageView.image = UIImage(named: String((dress?.image[indexPath.row])!)) //URL(string: "https://www.chichiclothing.com/media/catalog/product/7/3/7385nb-_1_copy__34601_std.jpg?quality=80&bg-color=255,255,255&fit=bounds&height=820&width=613")
//        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if dress?.image.count == 1 {
            pageControl.isHidden = true
        }else{
            pageControl.numberOfPages = dress?.image.count ?? 0
        }
        return dress?.image.count ?? 0
    }
}

extension DressViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

