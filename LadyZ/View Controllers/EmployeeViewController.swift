//
//  EmployeeViewController.swift
//  LadieZ
//
//  Created by Admin on 9/24/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class EmployeeViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var iamgeCollectionView: UICollectionView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var ReserveButton: UIButton!
    @IBOutlet weak var info: UILabel!
    
    
    var employee : Employee? {
        didSet {
            guard let _employee : Employee = self.employee else{
                return
            }
            set(_employee)
        }
    }
    func set (_ employee : Employee) {
        self.view.layoutIfNeeded()
        guard let _ = view else {
            print("viewNOT!")
            return
        }
        //image.image = UIImage(named: employee.image)
        name.text = employee.firstName + " " + employee.lastName
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        iamgeCollectionView.delegate = self
        iamgeCollectionView.dataSource = self
        scrollView.delegate = self
        
        ReserveButton.setGradient(from: UIColor(red: 0.91, green: 0.25, blue: 0.58, alpha: 1), to: UIColor(red: 0.96, green: 0.31, blue: 0.37, alpha: 1))
        
        if #available(iOS 11.0, *) {
            let a = UIApplication.shared.statusBarFrame.size.height
            let b = navigationController?.navigationBar.frame.height ?? 44
            scrollView.contentInset.top = -b//(a + b)
        }else {
            
        }
    }
    


}
extension EmployeeViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ImageCollectionViewCell
//        cell?.imageView.image = UIImage(named: String((employee?.image)!))
//        return cell!
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let size = collectionView.frame
        let imageview:UIImageView=UIImageView(frame: CGRect(x: size.minX, y: size.minY, width: size.size.width, height: size.size.height));

        let img : UIImage = UIImage(named:String((employee?.image)!))!
            imageview.image = img

            cell.contentView.addSubview(imageview)

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
       // self.pageControl.currentPage = indexPath.row
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if employee?.image.count == 1 {
//            //employee.isHidden = true
//        }else{
//            //pageControl.numberOfPages = employee?.image.count ?? 0
//        }
        return 1 //employee?.image.count ?? 0
    }
}

extension EmployeeViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
