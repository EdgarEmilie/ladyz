//
//  EmployeeCollectionViewController.swift
//  LadieZ
//
//  Created by Admin on 9/24/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class EmployeesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var empl0 : [Employee] = [Employee.init(_firstName: "Tina", _lastName: "Meliqsetyan", _image: "Tina")]
    var empl1 : [Employee] = [Employee.init(_firstName: "Ofelia", _lastName: "Grigoryan", _image: "Ofelia")]
    var empl2 : [Employee] = [Employee.init(_firstName: "Rita", _lastName: "Surenyan", _image: "Rita")]
    var param : String? {
        didSet {
            guard let _param : String = self.param else{
                return
            }
            set(_param)
        }
    }
    func set(_ param : String){
        if param == "0" {
            
        }else if param == "1"{
            
        }else if param == "2"{
            
        }
    }
    struct Storyboard {
        static let cell = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(UINib(nibName: "EmployeeCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        setupNavigationController()
        // Animate up to statusbar.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 2 - 14
        return CGSize(width: width , height: width + 28)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if param == "0" {
            return empl0.count
        } else if param == "1" {
            return empl1.count
        }else if param == "2" {
            return empl2.count
        }else {
            return 0
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? EmployeeCell else {
            return UICollectionViewCell()
        }
        //        cell.layer.cornerRadius = 10
        //        cell.layer.masksToBounds = true
        cell.backgroundColor = .white
        if param == "0" {
            cell.employee = empl0[indexPath.row]
        } else if param == "1" {
            cell.employee = empl1[indexPath.row]
        }else if param == "2" {
            cell.employee = empl2[indexPath.row]
        }
        return cell
    }
    
    
    
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if param == "0" {
            LadieZNavigationManager.instance.openEmployee(empl0[indexPath.row])
        } else if param == "1" {
            LadieZNavigationManager.instance.openEmployee(empl1[indexPath.row])
        }else if param == "2" {
            LadieZNavigationManager.instance.openEmployee(empl2[indexPath.row])
        }
        return true
    }
    
   func setupNavigationController() {
        //        let yourBackImage = UIImage(named: "chevron-left")
        //        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        //        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        //        self.navigationController?.navigationBar.backItem?.title = " "
        //        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        
        //        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        //let image = UIImage(named: "back")
        //UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.clear]
        
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage() , for:UIBarMetrics.default) //= UIColor(red:0.40, green:0.37, blue:1.00, alpha:0.2)
        self.navigationItem.title = " "//self.navigationItem.title = Swifternalization.localizedString("home")
        //self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = colour.backgraundColor
    
        //
        //statusBar.statusBarColor()
        
        //self.navigationController?.navigationBar.setGradient(from: colour.purpleColor.withAlphaComponent(0.3), to: colour.purpleColor.withAlphaComponent(0.0))
        
    }

}
