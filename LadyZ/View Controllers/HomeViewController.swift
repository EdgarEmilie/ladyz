//
//  HomeViewController.swift
//  LadieZ
//
//  Created by Admin on 7/19/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Lady'Z"
        setupNavigationController()
        scrollView.backgroundColor = colour.backgraundColor
        navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func setupNavigationController() {
        //        let yourBackImage = UIImage(named: "chevron-left")
        //        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        //        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        //        self.navigationController?.navigationBar.backItem?.title = " "
        //        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        
        //        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        //let image = UIImage(named: "back")
        //UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.clear]
        
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage() , for:UIBarMetrics.default) //= UIColor(red:0.40, green:0.37, blue:1.00, alpha:0.2)
        //self.navigationItem.title = " "//self.navigationItem.title = Swifternalization.localizedString("home")
        //self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = colour.backgraundColor
        
        //
        //statusBar.statusBarColor()
        
        //self.navigationController?.navigationBar.setGradient(from: colour.purpleColor.withAlphaComponent(0.3), to: colour.purpleColor.withAlphaComponent(0.0))
        
    }

}
