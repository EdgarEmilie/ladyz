//
//  LadieZTabViewController.swift
//  LadieZ
//
//  Created by Admin on 7/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LadieZTabViewController: UITabBarController {

    var tabBarItems = UITabBarItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : colour.buttonColor], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.darkGray], for: .normal)
        if self.tabBar.items != nil {
            let selectedImage = UIImage(named: "home")?.withRenderingMode(.alwaysTemplate)
            let deSelectedImage = UIImage(named: "home")?.withRenderingMode(.alwaysOriginal)
            tabBarItems = self.tabBar.items![0]
            tabBarItems.title = "HOME"
            tabBarItems.image = deSelectedImage
            tabBarItems.selectedImage = selectedImage
            
//            let selectedImage1 = UIImage(named: "Salon")?.withRenderingMode(.alwaysTemplate)
//            let deSelectedImage1 = UIImage(named: "Salon")?.withRenderingMode(.alwaysOriginal)
//            tabBarItems = self.tabBar.items![1]
//            tabBarItems.title = "SALON"
//            tabBarItems.image = deSelectedImage1
//            tabBarItems.selectedImage = selectedImage1
            
            let selectedImage2 = UIImage(named: "Group")?.withRenderingMode(.alwaysTemplate)
            let deSelectedImage2 = UIImage(named: "Group")?.withRenderingMode(.alwaysOriginal)
            tabBarItems = self.tabBar.items![1]
            tabBarItems.title = "DRESSES"
            tabBarItems.image = deSelectedImage2
            tabBarItems.selectedImage = selectedImage2
            
            let selectedImage3 = UIImage(named: "Group")?.withRenderingMode(.alwaysTemplate)
            let deSelectedImage3 = UIImage(named: "Group")?.withRenderingMode(.alwaysOriginal)
            tabBarItems = self.tabBar.items![2]
            tabBarItems.title = "ACCESSORIES"
            tabBarItems.image = deSelectedImage3
            tabBarItems.selectedImage = selectedImage3
            
            let selectedImage4 = UIImage(named: "acount")?.withRenderingMode(.alwaysTemplate)
            let deSelectedImage4 = UIImage(named: "acount")?.withRenderingMode(.alwaysOriginal)
            tabBarItems = self.tabBar.items![3]
            tabBarItems.title = "ACOUNT"
            tabBarItems.image = deSelectedImage4
            tabBarItems.selectedImage = selectedImage4
            tabBarItems.badgeValue = "2"
            if #available(iOS 10.0, *) {
                tabBarItems.badgeColor = colour.redColor
                tabBarItems.setBadgeTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], for: .normal)
            } else {
                // Fallback on earlier versions
            }
            // Do any additional setup after loading the view.
            
            let numberOfTabs = CGFloat((tabBar.items?.count)!)
            let tabBarSize = CGSize(width: tabBar.frame.width / numberOfTabs, height: tabBar.frame.height)
            tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: .clear, size: tabBarSize)
            
            self.selectedIndex = 0
        }
        self.tabBar.isTranslucent = false
        UITabBar.appearance().barTintColor = colour.backgraundColor // your color
        setupNavigationController()
    }
    
    func setupNavigationController() {
        //        let yourBackImage = UIImage(named: "chevron-left")
        //        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        //        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        //        self.navigationController?.navigationBar.backItem?.title = " "
        //        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        
        //        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        //let image = UIImage(named: "back")
        //UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.clear]
        
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage() , for:UIBarMetrics.default) //= UIColor(red:0.40, green:0.37, blue:1.00, alpha:0.2)
        self.navigationItem.title = " "//self.navigationItem.title = Swifternalization.localizedString("home")
        //self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = colour.backgraundColor
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = .black
        
        //
        //statusBar.statusBarColor()
        
        //self.navigationController?.navigationBar.setGradient(from: colour.purpleColor.withAlphaComponent(0.3), to: colour.purpleColor.withAlphaComponent(0.0))
        
    }

}
