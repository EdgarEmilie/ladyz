//
//  LanguageTableViewController.swift
//  VR_Ticket
//
//  Created by Admin on 2/23/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    let languages : [String] = ["English", "Русский", "Hayeren"]//,"Chinese (Simplified)","Chinese (Traditional)"]//,"Chinese (Hong Kong)"]
    var tabBar = UITabBar()
    var row = 0
    @IBOutlet weak var tableView: UITableView!
    //let statusBar = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = Swifternalization.localizedString("language")
        tableView.tintColor = colour.buttonColor
        tableView.bounces = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = colour.backgraundColor
        view.backgroundColor = colour.backgraundColor
        //statusBar.statusBarColor()
        if Globals.sharedInstance.selectedLanguage == "en"{
            row = 0
        }
        else if Globals.sharedInstance.selectedLanguage == "ru"{
            row = 1
        }
        else if Globals.sharedInstance.selectedLanguage == "am"{
            row = 2
        }
        
        //let index = IndexPath(row: row, section: 0)
        
    }
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return languages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        cell.textLabel?.text = languages[indexPath.row]
        cell.textLabel?.textColor = UIColor.darkGray
        cell.backgroundColor = colour.backgraundColor
        cell.selectionStyle = .none
        if indexPath.row == row {
            cell.accessoryType = .checkmark
        }else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) {
            resetChecks()
            cell.accessoryType = .checkmark
        }
        //tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        if(indexPath.row == 0){
            Globals.sharedInstance.selectedLanguage = "en"
        }
        else if(indexPath.row  == 1){
            Globals.sharedInstance.selectedLanguage = "ru"
        }
        else if(indexPath.row  == 2){
            Globals.sharedInstance.selectedLanguage = "am"
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    func resetChecks() {
        for i in 0..<tableView.numberOfSections {
            for j in 0..<tableView.numberOfRows(inSection: i) {
                if let cell = tableView.cellForRow(at: IndexPath(row: j, section: i)) {
                    cell.accessoryType = .none
                }
            }
        }
    }
   

   

}
