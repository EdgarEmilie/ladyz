//
//  SalonTableViewController.swift
//  LadieZ
//
//  Created by Admin on 7/19/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class SalonCatTableViewController: UITableViewController {
    private var cat: [Category] = [Category.init(_image: "beauty-salon", _title: "Beauty Salon", _tipe: "0"),Category.init(_image: "makeup", _title: "Makeup", _tipe: "1"),Category.init(_image: "nails", _title: "Nails", _tipe: "2")]
    override func viewDidLoad() {
        
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        // self.clearsSelectionOnViewWillAppear = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = colour.backgraundColor
    }
    
    // MARK: - Table view data source
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return cat.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CategoryTableViewCell
        cell.category = cat[indexPath.row]
        // Configure the cell...
        cell.backgroundColor = colour.backgraundColor
        cell.selectionStyle = .none
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LadieZNavigationManager.instance.openEmployeeCollection(cat[indexPath.row].tipe)
    }
}
