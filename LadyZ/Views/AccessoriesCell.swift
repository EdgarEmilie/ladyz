//
//  AccessoriesCell.swift
//  LadieZ
//
//  Created by Admin on 7/31/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AccessoriesCell: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var dressImage: UIImageView!
    @IBOutlet weak var bottomPrice: strikethroughLabel!
    @IBOutlet weak var topPrice: UILabel!
    @IBOutlet weak var favorite: UIButton!
    //@IBOutlet weak var rating: CosmosView!
    
    
    var accessory : Accessory? {
        didSet {
            self.layoutIfNeeded()
            guard let _ac : Accessory = self.accessory else{
                return
            }
            
            dressImage.isHidden = false
            dressImage.image = UIImage(named: _ac.image[0])
            if _ac.newPrice != nil {
                topPrice.text = String(_ac.newPrice!) + "AMD"
                topPrice.textColor = colour.buttonColor
                //oldPrice.text = String(_ac.oldPrice) + "AMD"
                
                let attributedString = NSMutableAttributedString(string:String(_ac.price) + "AMD")
                attributedString.addAttribute(NSAttributedString.Key.baselineOffset, value: 0, range: NSMakeRange(0, attributedString.length))
                attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.thick.rawValue), range: NSMakeRange(0, attributedString.length))
                attributedString.addAttribute(NSAttributedString.Key.strikethroughColor, value: colour.textColor, range: NSMakeRange(0, attributedString.length))
                
                bottomPrice.attributedText = attributedString
            }else {
                topPrice.text = String(_ac.price) + "AMD"
                bottomPrice.isHidden = true
            }
            
            //title.text = _dress.title
            setFavorite(isFav: _ac.isFavorite)
        }
    }
    
    @IBAction func changeFavorite(_ sender: Any) {
        guard var _dress : Accessory = self.accessory else{
            return
        }
        setFavorite(isFav: !_dress.isFavorite)
    }
    func setFavorite (isFav : Bool) {
        if isFav{
            favorite.setTitle("♥", for: .normal)
        }else {
            favorite.setTitle("♡", for: .normal)
        }
    }
    //    var dress: Dress? {
    //        didSet {
    //            if dressView != nil {
    //                self.layoutIfNeeded()
    //                dressView.dress = self.dress
    //            }
    //        }
    //    }♡♥
    override func awakeFromNib() {
        super.awakeFromNib()
        dressImage.layer.masksToBounds = true
    }

}
