//
//  ViewController.swift
//  myCalender2
//
//  Created by Muskan on 10/22/17.
//  Copyright © 2017 akhil. All rights reserved.
//

import UIKit



class CalendarViewController: UIViewController{
    
    //var theme = MyTheme.dark
    var time : String = "" {
        didSet {
//            guard let _ = self.time else {
//                return
//            }
            change()
        }
        
    }
    var h : String = ""{
        didSet {
            time = "\(h):\(m)"
        }
    }
    var m : String = "" {
        didSet {
            time = "\(h):\(m)"
        }
    }
    var day : String = "" {
        didSet {
            onConfirmeButton.isEnabled = true
//            guard let _ = self.time else {
//                return
//            }
            change()
        }
    }
    var month : String = "" {
        didSet {
            day = "__"
//            guard let _ = self.time else {
//                return
//            }
            change()
        }
    }
    
    
    static var instance : CalendarViewController? = CalendarViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.title = ""
//        self.navigationController?.navigationBar.isTranslucent=false
//        self.view.backgroundColor=Style.bgColor
        
       
        CalendarViewController.instance = self
        
        let monthsArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        let currentMonthIndex = Calendar.current.component(.month, from: Date()) - 1
        month = "\(monthsArr[currentMonthIndex])"
        day = "__"//String(Calendar.current.component(.day, from: Date()))
        //onConfirmeButton.isEnabled = false
//  
//        view.addSubview(calenderView)
//        view.addSubview(onConfirmeButton)
//        
//        
//        calenderView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive=true
//        calenderView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive=true
//        calenderView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 12).isActive=true
//        calenderView.bottomAnchor.constraint(equalTo: onConfirmeButton.topAnchor, constant: -12).isActive=true
//        //calenderView.heightAnchor.constraint(equalToConstant: 335).isActive=true
//        
//        //onConfirmeButton.topAnchor.constraint(equalTo: calenderView.bottomAnchor, constant: 0).isActive=true
//        onConfirmeButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive=true
//        onConfirmeButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 12).isActive=true
//        onConfirmeButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -12).isActive=true
//        onConfirmeButton.heightAnchor.constraint(equalToConstant: 45).isActive=true
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        calenderView.myCollectionView.collectionViewLayout.invalidateLayout()
    }

    
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
         
        dateFormatter.dateStyle = .none
        dateFormatter.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        dateFormatter.timeStyle = .short
        
        time = dateFormatter.string(from: sender.date)
        
        if time == "12:00" || time == "13:00" || time == "12:30" {
//            datePickerView.subviews[0].subviews[1].backgroundColor = UIColor.red
//            datePickerView.subviews[0].subviews[2].backgroundColor = UIColor.red
            
//            PickerView.subviews[0].subviews[1].backgroundColor = UIColor.red
//            PickerView.subviews[0].subviews[2].backgroundColor = UIColor.red
            onConfirmeButton.isEnabled = false
            onConfirmeButton.setTitleColor(.red, for: .normal)
            onConfirmeButton.alpha = 0.7
        }else {
//            datePickerView.subviews[0].subviews[1].backgroundColor = UIColor.black
//            datePickerView.subviews[0].subviews[2].backgroundColor = UIColor.black
            
//            PickerView.subviews[0].subviews[1].backgroundColor = UIColor.black
//            PickerView.subviews[0].subviews[2].backgroundColor = UIColor.black
            onConfirmeButton.isEnabled = true
            onConfirmeButton.setTitleColor(.white, for: .normal)
            onConfirmeButton.alpha = 1.0
        }
        //print(time)
    }
    public func change() {
        self.view.layoutIfNeeded()
               guard let _ = view else {
                   print("viewNOT!")
                   return
        }
        onConfirmeButton.setTitle(self.month + " / " + self.day + " / " + self.time, for: .normal)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        CalendarViewController.instance = nil
    }
   
    
   
    @objc func onConfirme() {
        print(self.month + " " + self.day + " " + self.time)
    }

    
    
    let calenderView: CalenderView = {
        let v = CalenderView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    let onConfirmeButton: UIButton = {
    let b = UIButton()
        b.backgroundColor = colour.darkColor
        b.translatesAutoresizingMaskIntoConstraints = false
        b.layer.cornerRadius = 16
        b.layer.masksToBounds = true
        //b.titleLabel?.text = "_ _ _ - __:__"
        b.addTarget(self, action: #selector(onConfirme), for: UIControl.Event.touchUpInside)
        b.titleLabel?.font = .boldSystemFont(ofSize: 18)
        b.setTitleColor(UIColor.lightGray, for: .disabled)

       return b
    }()
    
}

