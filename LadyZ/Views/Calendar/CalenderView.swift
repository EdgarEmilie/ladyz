//
//  CalenderView.swift
//  myCalender2
//
//  Created by Muskan on 10/22/17.
//  Copyright © 2017 akhil. All rights reserved.
//

import UIKit

struct Style {
    static var bgColor = UIColor.white
    static var monthViewLblColor = UIColor.black
    static var monthViewBtnRightColor = UIColor.black
    static var monthViewBtnLeftColor = UIColor.black
    static var activeCellLblColor = UIColor.black
    static var activeCellLblColorHighlighted = UIColor.white
    static var weekdaysLblColor = UIColor.black
    
  
}

class CalenderView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIPickerViewDelegate, UIPickerViewDataSource , MonthViewDelegate {
    
    let hour = ["9","10","11","12","13","14","15","16","17","18","19","20","21"]
    let minute = ["00","10","20","30","40","50"]
    var numOfDaysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31]
    var currentMonthIndex: Int = 0
    var currentYear: Int = 0
    var presentMonthIndex = 0
    var presentYear = 0
    var todaysDate = 0
    var firstWeekDayOfMonth = 0   //(Sunday-Saturday 1-7)
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initializeView()
    }
    
//    convenience init() {
//        self.init()
//        initializeView()
//    }
    
    func changeTheme() {
        myCollectionView.reloadData()
        
        monthView.lblName.textColor = Style.monthViewLblColor
        monthView.btnRight.setTitleColor(Style.monthViewBtnRightColor, for: .normal)
        monthView.btnLeft.setTitleColor(Style.monthViewBtnLeftColor, for: .normal)
        
        for i in 0..<7 {
            (weekdaysView.myStackView.subviews[i] as! UILabel).textColor = Style.weekdaysLblColor
        }
    }
    
    func initializeView() {
        currentMonthIndex = Calendar.current.component(.month, from: Date())
        currentYear = Calendar.current.component(.year, from: Date())
        todaysDate = Calendar.current.component(.day, from: Date())
        firstWeekDayOfMonth=getFirstWeekDay()
        
        //for leap years, make february month of 29 days
        if currentMonthIndex == 2 && currentYear % 4 == 0 {
            numOfDaysInMonth[currentMonthIndex-1] = 29
        }
        //end
        
        presentMonthIndex=currentMonthIndex
        presentYear=currentYear
        
        myCollectionView.delegate=self
        myCollectionView.dataSource=self
        PickerView.delegate = self
        PickerView.dataSource = self
        setupViews()
        
        myCollectionView.register(dateCVCell.self, forCellWithReuseIdentifier: "Cell")
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return numOfDaysInMonth[currentMonthIndex-1] + firstWeekDayOfMonth - 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! dateCVCell
        cell.backgroundColor=UIColor.clear
        
        if indexPath.item <= firstWeekDayOfMonth - 2 {
            cell.isHidden=true
        } else {
            let calcDate = indexPath.row-firstWeekDayOfMonth+2
            cell.isHidden=false
            cell.lbl.text="\(calcDate)"
            cell.layer.borderWidth = 0
            cell.layer.borderColor = UIColor.clear.cgColor
            if calcDate < todaysDate && currentYear == presentYear && currentMonthIndex == presentMonthIndex {
                cell.isUserInteractionEnabled=false
                cell.lbl.textColor = UIColor.lightGray
            } else {
                cell.isUserInteractionEnabled=true
                cell.lbl.textColor = Style.activeCellLblColor
//                if "\(calcDate)" == (ViewController.instance?.day)! {
//                    cell.isSelected = true
//                }
//                if cell.isSelected {
//                    cell.backgroundColor = Colors.selected
//                    let lbl = cell.subviews[1] as! UILabel
//                    lbl.textColor = UIColor.white
//                    ViewController.instance?.day = lbl.text!
//                }else {
//                    cell.backgroundColor=UIColor.clear
//                    let lbl = cell.subviews[1] as! UILabel
//                    lbl.textColor = Style.activeCellLblColor
//                }
            }
            if currentYear == 2019 && currentMonthIndex == 11 {
                if calcDate == 15 || calcDate == 19 || calcDate == 22 {
                    cell.layer.borderColor = UIColor.red.cgColor
                    cell.layer.borderWidth = 1
                    cell.isUserInteractionEnabled=false
                    cell.lbl.textColor = UIColor.lightGray
                }
            }
            if currentYear == 2019 && currentMonthIndex == 12 {
                if calcDate == 4 || calcDate == 9 || calcDate == 27 {
                    cell.layer.borderColor = UIColor.red.cgColor
                    cell.layer.borderWidth = 1
                    cell.isUserInteractionEnabled=false
                    cell.lbl.textColor = UIColor.lightGray
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell=collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = colour.buttonColor
        let lbl = cell?.subviews[1] as! UILabel
        lbl.textColor = Style.activeCellLblColor
        CalendarViewController.instance?.day = lbl.text!
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell=collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor=UIColor.clear
        let lbl = cell?.subviews[1] as! UILabel
        lbl.textColor = Style.activeCellLblColor
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width/7 //- 8
        let height: CGFloat = 40
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func getFirstWeekDay() -> Int {
        let day = ("\(currentYear)-\(currentMonthIndex)-01".date?.firstDayOfTheMonth.weekday)!
        return day == 1 ? 7 : day - 1
        
    }
    //UIPicker view delegate and datasorce functions
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            CalendarViewController.instance?.h = hour[row]
        case 1:
            CalendarViewController.instance?.m = minute[row]
        default:
            print("")
        }
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return hour.count
        case 1:
            return minute.count
        default:
            return NSNotFound
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return hour[row]
        case 1:
            return minute[row]
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var attributedString: NSAttributedString!

        switch component {
        case 0:
            attributedString = NSAttributedString(string: hour[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
        case 1:
            attributedString = NSAttributedString(string: minute[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
       
        default:
            attributedString = nil
        }

        return attributedString
    }

    
    func didChangeMonth(monthIndex: Int, year: Int) {
        currentMonthIndex=monthIndex+1
        currentYear = year
        
        //for leap year, make february month of 29 days
        if monthIndex == 1 {
            if currentYear % 4 == 0 {
                numOfDaysInMonth[monthIndex] = 29
            } else {
                numOfDaysInMonth[monthIndex] = 28
            }
        }
        //end
        
        firstWeekDayOfMonth=getFirstWeekDay()
        
        myCollectionView.reloadData()
        
        monthView.btnLeft.isEnabled = !(currentMonthIndex == presentMonthIndex && currentYear == presentYear)
    }
    
    func setupViews() {
        addSubview(monthView)
        monthView.topAnchor.constraint(equalTo: topAnchor).isActive=true
        monthView.leftAnchor.constraint(equalTo: leftAnchor).isActive=true
        monthView.rightAnchor.constraint(equalTo: rightAnchor).isActive=true
        monthView.heightAnchor.constraint(equalToConstant: 35).isActive=true
        monthView.delegate=self
        
        addSubview(weekdaysView)
        weekdaysView.topAnchor.constraint(equalTo: monthView.bottomAnchor).isActive=true
        weekdaysView.leftAnchor.constraint(equalTo: leftAnchor).isActive=true
        weekdaysView.rightAnchor.constraint(equalTo: rightAnchor).isActive=true
        weekdaysView.heightAnchor.constraint(equalToConstant: 30).isActive=true
        
        addSubview(myCollectionView)
        myCollectionView.topAnchor.constraint(equalTo: weekdaysView.bottomAnchor, constant: 0).isActive=true
        myCollectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive=true
        myCollectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive=true
        myCollectionView.heightAnchor.constraint(equalToConstant: 235).isActive = true
        //myCollectionView.bottomAnchor.constraint(equalTo: PickerView.topAnchor).isActive=true
        
        // Time selector <<
//        addSubview(PickerView)
//        PickerView.topAnchor.constraint(equalTo: myCollectionView.bottomAnchor, constant: 0).isActive=true
//        PickerView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive=true
//        PickerView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive=true
//        PickerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 40).isActive=true
//        PickerView.heightAnchor.constraint(equalToConstant: 200).isActive=true
        
        
        //MARK:TODO
//        if let vc = ViewController.instance?.day {
//            pickerView(PickerView, didSelectRow: 6, inComponent: 0)
//            pickerView(PickerView, didSelectRow: 0, inComponent: 1)
//            PickerView.selectRow(6, inComponent: 0, animated: false)
//            PickerView.selectRow(0, inComponent: 1, animated: false)
//        }
//
    }
//    func setTime() {
//            pickerView(PickerView, didSelectRow: 6, inComponent: 0)
//            pickerView(PickerView, didSelectRow: 0, inComponent: 1)
//            PickerView.selectRow(6, inComponent: 0, animated: false)
//            PickerView.selectRow(0, inComponent: 1, animated: false)
//    }
    
    let monthView: MonthView = {
        let v=MonthView()
        v.translatesAutoresizingMaskIntoConstraints=false
        return v
    }()
    
    let weekdaysView: WeekdaysView = {
        let v=WeekdaysView()
        v.translatesAutoresizingMaskIntoConstraints=false
        return v
    }()
    
    
    let myCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        let myCollectionView=UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        myCollectionView.showsHorizontalScrollIndicator = false
        myCollectionView.showsVerticalScrollIndicator = false
        myCollectionView.translatesAutoresizingMaskIntoConstraints=false
        myCollectionView.backgroundColor = UIColor.clear
        myCollectionView.isScrollEnabled = false
        myCollectionView.allowsMultipleSelection = false // TODO: if is dress change to true
        return myCollectionView
    }()
    let PickerView: UIPickerView = {
            let dp = UIPickerView()
            dp.translatesAutoresizingMaskIntoConstraints = false
    //        dp.numberOfComponents = 2
    //        dp.numberOfRows(inComponent: 0) = 10
    //        dp.numberOfRows(inComponent: 1) = 6
            dp.tintColor = .black
            dp.setValue(UIColor.black, forKey: "textColor")
            //for selector color
            //dp.subviews[0].subviews[1].backgroundColor = UIColor.black // top separator color
            //dp.subviews[0].subviews[2].backgroundColor = UIColor.black // bottom separator color
            return dp
        }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class dateCVCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor=UIColor.clear
        layer.cornerRadius=5
        layer.masksToBounds=true
        
        setupViews()
    }
    
    func setupViews() {
        addSubview(lbl)
        lbl.topAnchor.constraint(equalTo: topAnchor).isActive=true
        lbl.leftAnchor.constraint(equalTo: leftAnchor).isActive=true
        lbl.rightAnchor.constraint(equalTo: rightAnchor).isActive=true
        lbl.bottomAnchor.constraint(equalTo: bottomAnchor).isActive=true
    }
    
    let lbl: UILabel = {
        let label = UILabel()
        label.text = "00"
        label.textAlignment = .center
        label.font = UIFont.LadyzFont(name: .RobotoLight, size: 18)
        label.textColor = .darkGray
        label.translatesAutoresizingMaskIntoConstraints=false
        return label
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//get first day of the month
extension Date {
    var weekday: Int {
        return Calendar.current.component(.weekday, from: self)
    }
    var firstDayOfTheMonth: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year,.month], from: self))!
    }
}

//get date from string
extension String {
    static var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    var date: Date? {
        return String.dateFormatter.date(from: self)
    }
}













