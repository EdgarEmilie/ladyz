//
//  CartTableViewCell.swift
//  LadyZ
//
//  Created by Admin on 12/19/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    var accessory : Accessory? {
        didSet {
            self.layoutIfNeeded()
            guard let _ac : Accessory = self.accessory else{
                return
            }
            
            iamge.isHidden = false
            iamge.image = UIImage(named: _ac.image[0])
            title.text = "TITLE"
            
            if _ac.newPrice != nil {
                rightPrice.text = String(_ac.newPrice!) + "AMD"
                rightPrice.textColor = .black
                radioButton.isSelected = false
                
                leftPrice.text = String(_ac.price) + "AMD" // strikethroughText
                leftPrice.textColor = colour.textColor
            }else {
                rightPrice.text = String(_ac.price) + "AMD"
                leftPrice.isHidden = true
            }
            
        }
    }
    
    
    @IBOutlet weak var iamge: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var textsView: UIView!
    @IBOutlet weak var rightPrice: UILabel!
    @IBOutlet weak var leftPrice: strikethroughLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        title.font = UIFont.LadyzFont(name: .MontserratLight, size: 15)
        rightPrice.font = UIFont.LadyzFont(name: .RobotoBold, size: 16)
        leftPrice.font = UIFont.LadyzFont(name: .RobotoRegular, size: 15)
        
        
        view.backgroundColor = .white//colour.backgraundColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
