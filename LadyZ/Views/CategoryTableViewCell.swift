//
//  DressCatTableViewCell.swift
//  LadieZ
//
//  Created by Admin on 7/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var CatImage: UIImageView!
    @IBOutlet weak var CatTitle: UILabel!
    @IBOutlet weak var view: UIView!
    
    var category : Category?{
        didSet {
            CatImage.image = UIImage(named: (category?.image)!)
            CatTitle.text = category?.title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //view.backgroundColor = colour.backgraundColor
        view.layer.cornerRadius = 12
        view.layer.borderColor = UIColor.darkGray.cgColor//colour.backgraundColor.cgColor
        view.layer.borderWidth = 0.5
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 2, height: 3)
        view.layer.shadowRadius = 3
        
        CatTitle.font = UIFont.LadyzFont(name: .MontserratRegular, size: 22)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
