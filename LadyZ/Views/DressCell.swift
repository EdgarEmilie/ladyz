//
//  DressCollectionViewCell.swift
//  LadieZ
//
//  Created by Admin on 7/15/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Cosmos

class DressCell: UICollectionViewCell{
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var dressImage: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var favorite: UIButton!
    @IBOutlet weak var rating: CosmosView!
    
    var dress : Dress? {
        didSet {
            self.layoutIfNeeded()
            guard let _dress : Dress = self.dress else{
                return
            }
            dressImage.isHidden = false
            dressImage.image = UIImage(named: _dress.image[0])
            price.text = String(_dress.price) + "AMD"
            title.text = _dress.title
            setFavorite(isFav: _dress.isFavorite)
        }
    }
    @IBAction func changeFavorite(_ sender: Any) {
        guard var _dress : Dress = self.dress else{
            return
        }
        setFavorite(isFav: !_dress.isFavorite)
    }
    func setFavorite (isFav : Bool) {
        if isFav{
            favorite.setTitle("♥", for: .normal)
        }else {
            favorite.setTitle("♡", for: .normal)
        }
    }
    //    var dress: Dress? {
//        didSet {
//            if dressView != nil {
//                self.layoutIfNeeded()
//                dressView.dress = self.dress
//            }
//        }
//    }♡♥
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dressImage.layer.masksToBounds = true
    }
    
}
