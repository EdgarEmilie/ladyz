//
//  SalonCell.swift
//  LadieZ
//
//  Created by Admin on 9/24/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class EmployeeCell: UICollectionViewCell {
    var employee : Employee? {
        didSet {
            guard let _employee : Employee = self.employee else{
                return
            }
            name.text = _employee.firstName + " " + _employee.lastName
            image.image = UIImage(named: _employee.image)
        }
    }
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

}
