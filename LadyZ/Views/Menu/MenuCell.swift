//
//  MenuCell.swift
//  VR Ticket
//
//  Created by coder on 7/24/18.
//  Copyright © 2018 Hovhannes Tsakanyan. All rights reserved.
//

import UIKit


@IBDesignable
class MenuCellHolder: UIButton {
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var contentView:MenuCell?
    @IBInspectable var nibName:String?
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        xibSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    
    private func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view as? MenuCell
        setupContentView()
    }
    
    private func loadViewFromNib() -> UIView? {
        guard let nibName = nibName else { return nil }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
    }
    
    // Init view from storyboard
    private func setupContentView()
    {
        guard let _: MenuCell = self.contentView  else {
            return
        }
        self.text = { self.text }()
        self.text2 = { self.text2 }()
        self.icon = { self.icon }()
        self.image = { self.image}()
        self.badgeText = {self.badgeText}()
    }
    
    @IBInspectable
    var text: String = "" {
        didSet {
            contentView?.title.font = UIFont.LadyzFont(name: .RobotoRegular, size: 16)
            contentView?.title.text = text
            contentView?.title.textColor = colour.textColor
        }
    }
    var text2: String = "" {
        didSet {
            contentView?.title2.font = UIFont.LadyzFont(name: .RobotoRegular, size: 13)
            contentView?.title2.text = text2
            contentView?.title2.textColor = colour.textColor.withAlphaComponent(0.5)
        }
    }
    var badgeText: String = "" {
        didSet {
            if badgeText != "" {
                contentView?.badgeView.isHidden = false
                contentView?.badgeView.backgroundColor = colour.redColor
                contentView?.badgeView.layer.cornerRadius = 8//(contentView?.badgeView.frame.height)! / 2
                contentView?.badgeLabel.text = badgeText
                contentView?.badgeLabel.textColor = UIColor.white
            }else {
                contentView?.badgeView.isHidden = true
            }
        }
    }
    
    
    @IBAction func onSellected(_ sender: Any) {
        guard let page = LadieZNavigationManager.Page(rawValue: self.tag) else {
            return
        }
        LadieZNavigationManager.instance.open(page)
    }
    
    @IBInspectable
    public var icon: String? {
        didSet {
            guard let _:String = icon else {
                return
            }
            contentView?.icon.image = UIImage(named: icon!)
        }
    }
    public var image: String? {
        didSet {
            guard let _:String = image else {
                return
            }
            contentView?.image.image = UIImage(named: image!)
        }
    }
}


class MenuCell: UIView {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var button: HighlightedButton!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLabel: UILabel!
    
}
